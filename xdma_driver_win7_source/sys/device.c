/*
* XDMA Device
* ===========
* 
* Copyright 2017 Xilinx Inc.
* Copyright 2010-2012 Sidebranch
* Copyright 2010-2012 Leon Woestenberg <leon@sidebranch.com>
*
* Maintainer:
* -----------
* Alexander Hornburg <alexande@xilinx.com>
*
* References:
* -----------
*	[1] pg195-pcie-dma.pdf - DMA/Bridge Subsystem for PCI Express v3.0 - Product Guide
*   [2] Windows Dev - Using Automatic Synchronization - https://msdn.microsoft.com/en-us/windows/hardware/drivers/wdf/using-automatic-synchronization
*/

// ====================== include dependancies ========================================================

#include <ntddk.h>
#include <initguid.h> // required for GUID definitions
#include <wdmguid.h> // required for WMILIB_CONTEXT

#include "device.h"
#include "file_io.h"
#include "interrupt.h"
#include "dma_engine.h"
#include "xdma_public.h"

#include "trace.h"
#ifdef DBG
// The trace message header (.tmh) file must be included in a source file before any WPP macro 
// calls and after defining a WPP_CONTROL_GUIDS macro (defined in trace.h). see trace.h
#include "device.tmh"
#endif

// ====================== Function declarations ========================================================

// declare following functions as pageable code
#ifdef ALLOC_PRAGMA
#pragma alloc_text (PAGE, EvtDeviceAdd)
#pragma alloc_text (PAGE, EvtDevicePrepareHardware)
#pragma alloc_text (PAGE, EvtDeviceReleaseHardware)
#endif

// WDK 10 static code analysis feature expects to target Windows 10 and thus recommends not to
// use MmMapIoSpace and use MmMapIoSpaceEx instead. However this function is not available pre 
// Win 10. Thus disable this warning. 
// see https://social.msdn.microsoft.com/Forums/en-US/f8a3fb63-10de-481c-b629-8b5f3d254c5e/unexpected-code-analysis-behavior?forum=wdk
#pragma warning (disable : 30029) 

// ====================== constants ========================================================

// Version constants for the XMDA IP core
typedef enum XDMA_IP_VERSION_T {
    v2015_4 = 1,
    v2016_1 = 2,
    v2016_2 = 3,
    v2016_3 = 4,
    v2016_4 = 5,
    v2017_1 = 6,
} XDMA_IP_VERSION;

// ====================== static functions ========================================================

// Get the XDMA IP core version
static XDMA_IP_VERSION GetVersion(IN OUT PXDMA_DEVICE xdma) {
    XDMA_IP_VERSION version = xdma->configRegs->identifier & 0x000000ffUL;
    //TraceVerbose(DBG_INIT, "%!FUNC! version is 0x%x", version);
    return version;
}

// Iterate through PCIe resources and map BARS into host memory
static NTSTATUS MapBARs(IN WDFOBJECT device, IN WDFCMRESLIST ResourcesRaw,
                             IN WDFCMRESLIST ResourcesTranslated) {
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    PCM_PARTIAL_RESOURCE_DESCRIPTOR resource;
    PCM_PARTIAL_RESOURCE_DESCRIPTOR resourceRaw;
    ULONG numResources = WdfCmResourceListGetCount(ResourcesTranslated);
    NTSTATUS status = STATUS_SUCCESS;

    xdma->numBars = 0;

    TraceError(DBG_INIT, "%!FUNC! number of resources = %d", numResources);
    for (UINT i = 0; i < numResources; i++) {
        resource = WdfCmResourceListGetDescriptor(ResourcesTranslated, i);
        resourceRaw = WdfCmResourceListGetDescriptor(ResourcesRaw, i);
        if (!resource || !resourceRaw) {
            TraceError(DBG_INIT, "%!FUNC! WdfResourceCmGetDescriptor() fails");
            return STATUS_DEVICE_CONFIGURATION_ERROR;
        }

        switch (resource->Type) {
        case CmResourceTypePort:
            TraceVerbose(DBG_INIT, "%!FUNC! I/O mapped BAR: (%x) Length: (%d)",
                         resource->u.Port.Start.LowPart, resource->u.Port.Length);
            xdma->barIsMemory[xdma->numBars] = FALSE;
            xdma->barLength[xdma->numBars] = resource->u.Port.Length;
            xdma->bar[xdma->numBars] = ULongToPtr(resource->u.Port.Start.LowPart);
            xdma->numBars++;
            break;
        case CmResourceTypeMemory:
            xdma->barIsMemory[xdma->numBars] = TRUE;
            xdma->barLength[xdma->numBars] = resource->u.Memory.Length;
            xdma->bar[xdma->numBars] = MmMapIoSpace(resource->u.Memory.Start,
                                                    resource->u.Memory.Length, MmNonCached);
            if (xdma->bar[xdma->numBars] == NULL) {
                TraceError(DBG_INIT, "%!FUNC! MmMapIoSpace returned NULL! for BAR%u", xdma->numBars);
                return STATUS_DEVICE_CONFIGURATION_ERROR;
            }
            TraceVerbose(DBG_INIT, "%!FUNC! MM BAR %d (addr:0x%lld, length:%u) mapped at 0x%08p",
                         xdma->numBars, resource->u.Memory.Start.QuadPart,
                         resource->u.Memory.Length, xdma->bar[xdma->numBars]);
            xdma->numBars++;
            break;
        case CmResourceTypeInterrupt:
            TraceVerbose(DBG_INIT, "%!FUNC! PCIe %s Interrupt resource",
                         resource->Flags & CM_RESOURCE_INTERRUPT_MESSAGE ? "MSI/MSI-X" : "Line");
            xdma->numIrqResources++;
            break;
        default:
            TraceWarning(DBG_INIT, "%!FUNC! Unhandled Resource Type 0x%x", resource->Type);
            break;
        }

        if (!NT_SUCCESS(status)) {
            TraceError(DBG_INIT, "%!FUNC! error in handling resource!");
            break;
        }
    }

    return status;
}

// Is the BAR at index 'idx' the config BAR?
static BOOLEAN IsConfigBAR(IN PXDMA_DEVICE xdma, IN UINT idx) {

    XDMA_IRQ_REGS* irqRegs = (XDMA_IRQ_REGS*)((PUCHAR)xdma->bar[idx] + IRQ_BLOCK_OFFSET);
    XDMA_CONFIG_REGS* cfgRegs = (XDMA_CONFIG_REGS*)((PUCHAR)xdma->bar[idx] + CONFIG_BLOCK_OFFSET);

    UINT32 interruptID = irqRegs->identifier & XDMA_ID_MASK;
    UINT32 configID = cfgRegs->identifier & XDMA_ID_MASK;

    return ((interruptID == XDMA_ID) && (configID == XDMA_ID)) ? TRUE : FALSE;
}

// Identify which BAR is the config BAR
static UINT FindConfigBAR(IN PXDMA_DEVICE xdma) {
    for (UINT i = 0; i < xdma->numBars; ++i) {
        if (IsConfigBAR(xdma, i)) {
            TraceInfo(DBG_INIT, "%!FUNC! config BAR is %u", i);
            return i;
        }
    }
    return xdma->numBars; //not found - return past-the-end index
}

// Get the driver parameter for POLL_MODE from the Windows registry 
static NTSTATUS GetPollModeParameter(IN PXDMA_DEVICE xdma) {
    WDFDRIVER driver = WdfGetDriver();
    WDFKEY key;
    NTSTATUS status = WdfDriverOpenParametersRegistryKey(driver, STANDARD_RIGHTS_ALL,
                                                WDF_NO_OBJECT_ATTRIBUTES, &key);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfDriverOpenParametersRegistryKey failed: %!STATUS!", status);
        WdfRegistryClose(key);
        return status;
    }

    DECLARE_CONST_UNICODE_STRING(valueName, L"POLL_MODE");

    status = WdfRegistryQueryULong(key, &valueName, &xdma->pollMode);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfRegistryQueryULong failed: %!STATUS!", status);
        WdfRegistryClose(key);
        return status;
    }

    TraceVerbose(DBG_INIT, "%!FUNC! pollMode=%u", xdma->pollMode);

    WdfRegistryClose(key);
    return status;
}

// ====================== plug and play callback functions ========================================

// Perform device initialization when a device is found by the PnP manager
NTSTATUS EvtDeviceAdd(IN WDFDRIVER Driver, IN PWDFDEVICE_INIT DeviceInit) {
    NTSTATUS status = STATUS_SUCCESS;
    WDF_PNPPOWER_EVENT_CALLBACKS PnpPowerCallbacks;
    WDF_POWER_POLICY_EVENT_CALLBACKS powerPolicyCallbacks;
    WDF_OBJECT_ATTRIBUTES deviceAttributes;
    WDFDEVICE device;

    WDF_IO_QUEUE_CONFIG queueConfig;
    WDF_OBJECT_ATTRIBUTES fileAttributes;
    WDF_FILEOBJECT_CONFIG fileConfig;
    PXDMA_DEVICE xdma = NULL;
    WDF_DMA_ENABLER_CONFIG dmaConfig;
    ULONG bytesRead = 0;

    PAGED_CODE();

    TraceVerbose(DBG_INIT, "%!FUNC! (Driver=0x%p)", Driver);

    //  We prefer Direct I/O
    //  Direct I/O only works with deferred buffer retrieval No guarantee that Direct I/O is
    //  actually used Direct I/O is only used for buffers that are full pages Buffered I/O is used
    //  for other parts of the transfer
    WdfDeviceInitSetIoType(DeviceInit, WdfDeviceIoDirect);

    // Set call-backs for any of the functions we are interested in. If no call-back is set, the 
    // framework will take the default action by itself.
    WDF_PNPPOWER_EVENT_CALLBACKS_INIT(&PnpPowerCallbacks);
    PnpPowerCallbacks.EvtDevicePrepareHardware = EvtDevicePrepareHardware;
    PnpPowerCallbacks.EvtDeviceReleaseHardware = EvtDeviceReleaseHardware;
    WdfDeviceInitSetPnpPowerEventCallbacks(DeviceInit, &PnpPowerCallbacks);

    WDF_POWER_POLICY_EVENT_CALLBACKS_INIT(&powerPolicyCallbacks);
    WdfDeviceInitSetPowerPolicyEventCallbacks(DeviceInit, &powerPolicyCallbacks);

    // ContextCleanup will be called by the framework when it deletes the device. So you can defer
    // freeing any resources allocated to Cleanup callback in the event EvtDeviceAdd returns any 
    // error after the device is created.
    deviceAttributes.EvtCleanupCallback = EvtDeviceCleanup;

    // Register file object call-backs
    WDF_FILEOBJECT_CONFIG_INIT(&fileConfig, EvtDeviceFileCreate, EvtFileClose, EvtFileCleanup);
    WDF_OBJECT_ATTRIBUTES_INIT(&fileAttributes);
    fileAttributes.SynchronizationScope = WdfSynchronizationScopeNone;
    WDF_OBJECT_ATTRIBUTES_SET_CONTEXT_TYPE(&fileAttributes, FILE_CONTEXT);
    WdfDeviceInitSetFileObjectConfig(DeviceInit, &fileConfig, &fileAttributes);

    // Specify the context type and size for the device we are about to create.
    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&deviceAttributes, XDMA_DEVICE);

    status = WdfDeviceCreate(&DeviceInit, &deviceAttributes, &device);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfDeviceCreate failed: %!STATUS!", status);
        return status;
    }

    // get and default-initialize the device context
    xdma = GetDeviceContext(device);
    for (UINT32 i = 0; i < XDMA_MAX_NUM_BARS; i++) {
        xdma->bar[i] = NULL;
        xdma->barLength[i] = 0;
        xdma->barIsMemory[i] = FALSE;
    }
    xdma->numIrqResources = 0;

    // get bus interface
    status = WdfFdoQueryForInterface(device, &GUID_BUS_INTERFACE_STANDARD,
        (PINTERFACE)&xdma->pciBus, sizeof(BUS_INTERFACE_STANDARD), 1 /* Version */, NULL);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfFdoQueryForInterface failed: %!STATUS!", status);
        return status;
    }

    // Get device bus number
    status = IoGetDeviceProperty(WdfDeviceWdmGetPhysicalDevice(device), DevicePropertyBusNumber,
                                 sizeof(ULONG), &xdma->pciBusNr, &bytesRead);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! IoGetDeviceProperty(BusNumber) failed: %!STATUS!", status);
        return status;
    }

    // Get device address
    status = IoGetDeviceProperty(WdfDeviceWdmGetPhysicalDevice(device), DevicePropertyAddress,
                                 sizeof(ULONG), &xdma->pciFunctionNr, &bytesRead);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! GetDeviceProperty(Address) failed %!STATUS!", status);
        return status;
    }

    // For PCI, the DevicePropertyAddress has device number in the high word and the function 
    // number in the low word.
    xdma->pciDeviceNr = (xdma->pciFunctionNr >> 16) & 0x0000FFFF;
    xdma->pciFunctionNr &= 0x0000FFFF;

    bytesRead = 0;
    bytesRead = xdma->pciBus.GetBusData(xdma->pciBus.Context, PCI_WHICHSPACE_CONFIG, //READ
                                        &xdma->pciHeader, FIELD_OFFSET(PCI_COMMON_CONFIG, VendorID),
                                        PCI_COMMON_HDR_LENGTH);
    if ((LONG)bytesRead != PCI_COMMON_HDR_LENGTH) {
        TraceError(DBG_INIT, "%!FUNC! GetPciCommonHeader failed: got %d bytes instead of %d",
                   bytesRead, PCI_COMMON_HDR_LENGTH);
        return STATUS_INVALID_DEVICE_REQUEST;
    }
    
    // Tell the Framework that this device will need an interface so that applications can 
    // interact with it.
    status = WdfDeviceCreateDeviceInterface(device, (LPGUID)&GUID_DEVINTERFACE_XDMA, NULL);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfDeviceCreateDeviceInterface failed %!STATUS!", status);
        return status;
    }

    // accept multiple I/O request to run in parallel, they are sequentialized later
    WDF_IO_QUEUE_CONFIG_INIT_DEFAULT_QUEUE(&queueConfig, WdfIoQueueDispatchParallel);
    queueConfig.EvtIoDeviceControl = EvtIoDeviceControl; // callback handler for control requests
    queueConfig.EvtIoRead = EvtIoRead; // callback handler for read requests
    queueConfig.EvtIoWrite = EvtIoWrite; // callback handler for write requests

    // create the default queue upon all I/O requests arrive
    status = WdfIoQueueCreate(device, &queueConfig, WDF_NO_OBJECT_ATTRIBUTES, &xdma->entryQueue);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfIoQueueCreate failed: %!STATUS!", status);
        return status;
    }

    // Create the events read queue - separate from the others
    WDF_IO_QUEUE_CONFIG_INIT(&queueConfig, WdfIoQueueDispatchSequential);
    queueConfig.EvtIoRead = ServiceUserEvent; // callback handler for event read requests
    for (UINT i = 0; i < XDMA_MAX_USER_IRQ; ++i) {
        status = WdfIoQueueCreate(device, &queueConfig, WDF_NO_OBJECT_ATTRIBUTES,
                                  &xdma->userEvents[i].queue);
        if (!NT_SUCCESS(status)) {
            TraceError(DBG_INIT, "%!FUNC! WdfIoQueueCreate failed: %!STATUS!", status);
            return status;
        }

        // this queue should not forward requests until we ask it to
        WdfIoQueueStop(xdma->userEvents[i].queue, NULL, NULL);
    }

    // WDF DMA Enabler - at least 8 bytes alignment
    WdfDeviceSetAlignmentRequirement(device, 8 - 1); // TODO - choose correct value
    WDF_DMA_ENABLER_CONFIG_INIT(&dmaConfig, WdfDmaProfileScatterGather64Duplex, MAX_TRANSFER_SIZE);
    status = WdfDmaEnablerCreate(device, &dmaConfig, WDF_NO_OBJECT_ATTRIBUTES, &xdma->dmaEnabler);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC!  WdfDmaEnablerCreate() failed: %!STATUS!", status);
        return status;
    }

    TraceInfo(DBG_INIT, "%!FUNC! Added device at Bus %u Device %u",
              xdma->pciBusNr, xdma->pciDeviceNr);

    TraceVerbose(DBG_INIT, "%!FUNC! returns %!STATUS!", status);
    return status;
}

// Any device specific cleanup  - TODO device reset?
VOID EvtDeviceCleanup(IN WDFOBJECT device) {
    UNREFERENCED_PARAMETER(device);
    TraceInfo(DBG_INIT, "%!FUNC!");
}

// Initialize device hardware and host buffers.
// Called by plug and play manager
NTSTATUS EvtDevicePrepareHardware(IN WDFDEVICE device, IN WDFCMRESLIST Resources,
                                  IN WDFCMRESLIST ResourcesTranslated) {
    NTSTATUS status = STATUS_SUCCESS;
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    PUCHAR configBarAddr = NULL;

    PAGED_CODE();
    UNREFERENCED_PARAMETER(Resources);
    TraceVerbose(DBG_INIT, "%!FUNC!");

    // map PCIe BARs to host memory
    status = MapBARs(device, Resources, ResourcesTranslated);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! MapBARs() failed: %!STATUS!", status);
        return status;
    }

    // find DMA config BAR (usually BAR1, see section 'Target Bridge' in [1]) 
    xdma->configBarIdx = FindConfigBAR(xdma);
    if (xdma->configBarIdx == xdma->numBars) {
        TraceError(DBG_INIT, "%!FUNC! findConfigBar() failed: bar is %d", xdma->configBarIdx);
        return STATUS_DRIVER_INTERNAL_ERROR;
    }
    xdma->userBarIdx = xdma->configBarIdx == 1 ? 0 : -1; // if config bar is bar0 then user bar doesnt exit
    xdma->bypassBarIdx = xdma->numBars - xdma->configBarIdx == 2 ? xdma->numBars - 1 : -1; // if config bar is not the last bar then bypass bar exists

    TraceVerbose(DBG_INIT, "%!FUNC!, BAR index: user=%d, control=%d, bypass=%d", 
                 xdma->userBarIdx, xdma->configBarIdx, xdma->bypassBarIdx);

    // remember module offsets in config BAR
    configBarAddr = (PUCHAR)xdma->bar[xdma->configBarIdx];
    xdma->configRegs = (XDMA_CONFIG_REGS*)(configBarAddr + CONFIG_BLOCK_OFFSET);
    xdma->interruptRegs = (XDMA_IRQ_REGS*)(configBarAddr + IRQ_BLOCK_OFFSET);

    UINT version = GetVersion(xdma);
    if (version != v2017_1) {
        TraceWarning(DBG_INIT, "%!FUNC! potential version mismatch! Expected 2017.1 (0x6) but got (0x%x)", version);
    }

    // If configured as poll mode then we dont need to initialize dma engine interrupts
    status = GetPollModeParameter(xdma);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! GetPollModeParameter failed: %!STATUS!", status);
        return status;
    }
    if (!xdma->pollMode) {
        TraceVerbose(DBG_INIT, "%!FUNC! xdma->numIrqResources=%u", xdma->numIrqResources);
        if (xdma->numIrqResources == XMDA_MAX_NUM_IRQ) {
            status = SetupMultipleInterruptResources(device, Resources, ResourcesTranslated);
        } else {
            status = SetupSingleInterruptResource(device, Resources, ResourcesTranslated);
        }
        if (!NT_SUCCESS(status)) {
            TraceError(DBG_INIT, "%!FUNC! Setting up interrupts failed: %!STATUS!", status);
            return status;
        }
    }

    // Initialize an XDMA_ENGINE struct for every DMA engined configured in HW
    status = ProbeEngines(device, configBarAddr);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! ProbeEngines failed: %!STATUS!", status);
        return status;
    }

    TraceVerbose(DBG_INIT, "%!FUNC! returns %!STATUS!", status);
    return status;
}

// Unmap PCIe resources
NTSTATUS EvtDeviceReleaseHardware(IN WDFDEVICE Device, IN WDFCMRESLIST ResourcesTranslated) {

    PAGED_CODE();
    UNREFERENCED_PARAMETER(ResourcesTranslated);
    TraceInfo(DBG_INIT, "%!FUNC! entry");

    PXDMA_DEVICE xdma = GetDeviceContext(Device);
    ASSERTMSG(__FUNCTION__ "device context is NULL", xdma != NULL);

    // Unmap any I/O ports. Disconnecting from the interrupt will be done automatically by the framework.
    for (UINT i = 0; i < xdma->numBars; i++) {
        if (xdma->barIsMemory[i] && xdma->bar[i] != NULL) {
            TraceInfo(DBG_INIT, "%!FUNC! Unmapping BAR%d, VA:(%p) Length %ul",
                      i, xdma->bar[i], xdma->barLength[i]);
            MmUnmapIoSpace(xdma->bar[i], xdma->barLength[i]);
            xdma->bar[i] = NULL;
        }
    }

    TraceInfo(DBG_INIT, "%!FUNC! exit");
    return STATUS_SUCCESS;
}

