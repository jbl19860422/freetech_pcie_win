/*
* XDMA Device File Interfaces for public API
* ===============================
* 
* Copyright 2017 Xilinx Inc.
* Copyright 2010-2012 Sidebranch
* Copyright 2010-2012 Leon Woestenberg <leon@sidebranch.com>
*
* Maintainer:
* -----------
* Alexander Hornburg <alexande@xilinx.com>
*
* IO Request flow diagram:
* ------------------------
* User Operation (e.g. ReadFile())
* |
* |-> IO Request -> EvtIoRead()--> ReadBarToRequest()               // PCI BAR access
*               |            |---> EvtIoReadDma()                   // normal dma c2h transfer
*               |            |---> EvtIoReadEngineRing()            // for streaming interface
*               |            |---> CopyDescriptorsToRequestMemory() // get dma descriptors to user-space
*               |            |---> ServiceUserEvent()               // wait on user interrupt
*               |
*               |-> EvtIoWrite()-> WriteBarFromRequest()            // PCI BAR access
*                             |--> EvtIoWriteDma()                  // normal DMA H2C transfer
*                             |--> WriteBypassDescriptor()          // write descriptors from userspace to bypass BARs
*/

// ========================= include dependencies =================================================

#include "device.h"
#include "dma_engine.h"
#include "xdma_public.h"
#include "file_io.h"

#include "trace.h"
#ifdef DBG
// The trace message header (.tmh) file must be included in a source file before any WPP macro 
// calls and after defining a WPP_CONTROL_GUIDS macro (defined in trace.h). see trace.h
#include "file_io.tmh"
#endif

// ====================== device file node functions =========================================== 

const static struct {
    DEVNODE_TYPE devType;
    const wchar_t *wstr;
    ULONG channel;
} FileNameLUT[] = {
    { DEVNODE_TYPE_H2C,         XDMA_FILE_H2C_0,        0 },
    { DEVNODE_TYPE_C2H,         XDMA_FILE_C2H_0,        0 },
    { DEVNODE_TYPE_H2C,         XDMA_FILE_H2C_1,        1 },
    { DEVNODE_TYPE_C2H,         XDMA_FILE_C2H_1,        1 },
    { DEVNODE_TYPE_H2C,         XDMA_FILE_H2C_2,        2 },
    { DEVNODE_TYPE_C2H,         XDMA_FILE_C2H_2,        2 },
    { DEVNODE_TYPE_H2C,         XDMA_FILE_H2C_3,        3 },
    { DEVNODE_TYPE_C2H,         XDMA_FILE_C2H_3,        3 },
    { DEVNODE_TYPE_BYPASS_H2C,  XDMA_FILE_BYPASS_H2C_0, 0 },
    { DEVNODE_TYPE_BYPASS_H2C,  XDMA_FILE_BYPASS_H2C_1, 1 },
    { DEVNODE_TYPE_BYPASS_H2C,  XDMA_FILE_BYPASS_H2C_2, 2 },
    { DEVNODE_TYPE_BYPASS_H2C,  XDMA_FILE_BYPASS_H2C_3, 3 },
    { DEVNODE_TYPE_BYPASS_C2H,  XDMA_FILE_BYPASS_C2H_0, 0 },
    { DEVNODE_TYPE_BYPASS_C2H,  XDMA_FILE_BYPASS_C2H_1, 1 },
    { DEVNODE_TYPE_BYPASS_C2H,  XDMA_FILE_BYPASS_C2H_2, 2 },
    { DEVNODE_TYPE_BYPASS_C2H,  XDMA_FILE_BYPASS_C2H_3, 3 },
    { DEVNODE_TYPE_USER,        XDMA_FILE_USER,         0 },
    { DEVNODE_TYPE_CONTROL,     XDMA_FILE_CONTROL,      0 },
    { DEVNODE_TYPE_BYPASS,      XDMA_FILE_BYPASS,       0 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_0,      0 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_1,      1 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_2,      2 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_3,      3 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_4,      4 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_5,      5 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_6,      6 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_7,      7 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_8,      8 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_9,      9 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_10,     10 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_11,     11 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_12,     12 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_13,     13 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_14,     14 },
    { DEVNODE_TYPE_EVENTS,      XDMA_FILE_EVENT_15,     15 },
};

static VOID GetDevNodeType(IN PUNICODE_STRING fileName, IN OUT PFILE_CONTEXT file)
// convert from filename to device node
{
    for (UINT i = 0; i < sizeof(FileNameLUT) / sizeof(FileNameLUT[0]); ++i) {
        if (!wcscmp(fileName->Buffer, FileNameLUT[i].wstr)) {
            file->devType = FileNameLUT[i].devType;
            file->channel = FileNameLUT[i].channel;
            return;
        }
    }
    TraceError(DBG_IO, "%!FUNC! GetDevNodeID() returns ID_DEVNODE_UNKNOWN");
    file->devType = ID_DEVNODE_UNKNOWN;
}

VOID EvtDeviceFileCreate(IN WDFDEVICE device, IN WDFREQUEST Request, IN WDFFILEOBJECT WdfFile) {
    PUNICODE_STRING fileName = WdfFileObjectGetFileName(WdfFile);
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    PFILE_CONTEXT devNode = GetFileContext(WdfFile);
    NTSTATUS status = STATUS_SUCCESS;

    TraceInfo(DBG_IO, "%!FUNC!(Device=0x%p, Request=0x%p, FileObject=0x%p",
              device, Request, WdfFile);

    // no filename given?
    if (fileName == NULL) {
        TraceError(DBG_IO, "%!FUNC! failed: no filename given.");
        status = STATUS_INVALID_PARAMETER;
        goto ErrExit;
    }

    // device node has zero length?
    ASSERTMSG("fileName is empty string", fileName->Length != 0);
    GetDevNodeType(fileName, devNode);
    if (devNode->devType == ID_DEVNODE_UNKNOWN) {
        TraceError(DBG_IO, "%!FUNC! failed: invalid device node given: %wZ", fileName);
        status = STATUS_INVALID_PARAMETER;
        goto ErrExit;
    }

    // SGDMA device?
    if ((devNode->devType == DEVNODE_TYPE_H2C) || (devNode->devType == DEVNODE_TYPE_C2H)) {
        ULONG channel = devNode->channel;
        DirToDev dir = devNode->devType == DEVNODE_TYPE_H2C ? H2C : C2H;
        XDMA_ENGINE* engine = &(xdma->engines[channel][dir]);
        TraceVerbose(DBG_IO, "%!FUNC! engine[%d][%d].queue=%p",
                     channel, dir, engine->queue);

        if (engine->enabled == FALSE) {
            TraceError(DBG_IO, "%!FUNC! failed: xdma_%s_%d not enabled in XDMA IP core",
                       dir == H2C ? "h2c" : "c2h", channel);
            status = STATUS_INVALID_PARAMETER;
            goto ErrExit;
        }

        // copy pointer to write engine queue into the file context
        devNode->queue = engine->queue;
        TraceVerbose(DBG_IO, "%!FUNC! fileContext->queue=0x%p", devNode->queue);

        if ((engine->streaming == TRUE) && (dir == C2H)) {
            EngineRingSetup(engine);
        }
    } else if (devNode->devType == DEVNODE_TYPE_EVENTS) {
        devNode->queue = xdma->userEvents[devNode->channel].queue;
    } else if ((devNode->devType == DEVNODE_TYPE_BYPASS_H2C)
               || (devNode->devType == DEVNODE_TYPE_BYPASS_C2H)) {
        ULONG channel = devNode->channel;
        DirToDev dir = devNode->devType == DEVNODE_TYPE_BYPASS_H2C ? H2C : C2H;
        XDMA_ENGINE* engine = &(xdma->engines[channel][dir]);
        if (engine->enabled == FALSE) {
            TraceError(DBG_IO, "%!FUNC! failed: xdma_%s_%d not enabled in XDMA IP core",
                       dir == H2C ? "h2c" : "c2h", channel);
            status = STATUS_INVALID_PARAMETER;
            goto ErrExit;
        }
        devNode->queue = engine->queue; // TODO should engine be used here instead of queue?
    }
ErrExit:
    WdfRequestComplete(Request, status);
    TraceInfo(DBG_IO, "%!FUNC! returns %!STATUS!", status);
}

VOID EvtFileClose(IN WDFFILEOBJECT FileObject) {
    UNREFERENCED_PARAMETER(FileObject);
    TraceInfo(DBG_IO, "%!FUNC!");
}

VOID EvtFileCleanup(IN WDFFILEOBJECT FileObject) {
    UNREFERENCED_PARAMETER(FileObject);
    PFILE_CONTEXT file = GetFileContext(FileObject);
    TraceVerbose(DBG_IO, "%!FUNC!");
    if (file->devType == DEVNODE_TYPE_C2H) {
        PXDMA_DEVICE xdma = GetDeviceContext(WdfFileObjectGetDevice(FileObject));
        XDMA_ENGINE* engine = &(xdma->engines[file->channel][C2H]);
        if (engine->streaming == TRUE) {
            EngineRingTeardown(engine);
        }

        // we could be processing an interrupt while closing, in this case:
        //	1. the isr disables the channel interrupt
        //  2. the irq dpc will not be called (therefore interrupt is not reenabled)
        //  3. we need to manually enable the interrupt again. 
        EngineEnableInterrupt(engine);
    }
    TraceInfo(DBG_IO, "%!FUNC!");
}

// ====================== IO Request callback functions ===========================================

static NTSTATUS ValidateBarParams(IN PXDMA_DEVICE xdma, ULONG nBar, size_t offset, size_t length) {
    if (length == 0) {
        TraceError(DBG_IO, "%!FUNC! failed: attempting to read 0 bytes");
        return STATUS_INVALID_DEVICE_REQUEST;
    }

    // invalid BAR index?
    if (nBar >= xdma->numBars) {
        TraceError(DBG_IO, "%!FUNC! failed: attempting to read BAR %u but only 2 exist", nBar);
        return STATUS_INVALID_DEVICE_REQUEST;
    }

    // access outside valid BAR address range?
    if (offset + length >= xdma->barLength[nBar]) {
        TraceError(DBG_IO, "%!FUNC! failed: attempting to read BAR %u offset=%llu size=%llu",
                   nBar, offset, length);
        return STATUS_INVALID_DEVICE_REQUEST;
    }
    return STATUS_SUCCESS;
}

static NTSTATUS ReadBarToRequest(IN PXDMA_DEVICE xdma, IN WDFREQUEST request, IN UINT32 nBar)
// Read from PCIe mmap'ed memory into an IO request 
{
    NTSTATUS status = STATUS_SUCCESS;
    WDF_REQUEST_PARAMETERS params;
    size_t offset;
    size_t length;
    PUCHAR readAddr;
    WDFMEMORY requestMemory;

    WDF_REQUEST_PARAMETERS_INIT(&params);
    WdfRequestGetParameters(request, &params);
    offset = (size_t)params.Parameters.Read.DeviceOffset;
    length = params.Parameters.Read.Length;

    TraceVerbose(DBG_IO, "%!FUNC! xdma=%p, request=%p", xdma, request);
    TraceVerbose(DBG_IO, "%!FUNC! reading %llu bytes at BAR %u at offset 0x%llx",
                 length, nBar, offset);

    status = ValidateBarParams(xdma, nBar, offset, length);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! ValidateBarParams failed: %!STATUS!", status);
        return status;
    }

    // Static Driver Verifier is not smart enough to see that length is checked in ValidateBarParams
    // Therefore we need to check it here as well
    if (length == 0) {
        TraceError(DBG_IO, "%!FUNC! failed: attempting to read 0 bytes");
        return STATUS_INVALID_DEVICE_REQUEST;
    }

    // calculate virtual address of the mmap'd BAR location
    readAddr = (PUCHAR)xdma->bar[nBar] + offset;

    // get handle to the IO request memory which will hold the read data
    status = WdfRequestRetrieveOutputMemory(request, &requestMemory);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfRequestRetrieveOutputMemory failed: %!STATUS!", status);
        return status;
    }

    // copy from mmap'd BAR into request memory
	unsigned int user_val = 0;
	if ((UINT32)xdma->userBarIdx == nBar)
	{
		user_val = *(unsigned int *)readAddr;
		status = WdfMemoryCopyFromBuffer(requestMemory, 0, (PUCHAR)&user_val, 4);
		if (!NT_SUCCESS(status)) {
			TraceError(DBG_IO, "%!FUNC! WdfMemoryCopyToBuffer() failed : %!STATUS!", status);
			return status;
		}
		
		status = STATUS_SUCCESS;
	}
	else
	{
		status = WdfMemoryCopyFromBuffer(requestMemory, 0, readAddr, length);
		if (!NT_SUCCESS(status)) {
			TraceError(DBG_IO, "%!FUNC! WdfMemoryCopyFromBuffer failed: %!STATUS!", status);
			return status;
		}
	}

    return status;
}

static NTSTATUS WriteBarFromRequest(IN PXDMA_DEVICE xdma, IN WDFREQUEST request, IN UINT32 nBar)
// Write from an IO request into PCIe mmap'ed memory 
{
    NTSTATUS status = STATUS_SUCCESS;
    WDF_REQUEST_PARAMETERS params;
    size_t offset;
    size_t length;
    PUCHAR writeAddr;

    WDF_REQUEST_PARAMETERS_INIT(&params);
    WdfRequestGetParameters(request, &params);
    offset = (size_t)params.Parameters.Read.DeviceOffset;
    length = params.Parameters.Read.Length;

    TraceVerbose(DBG_IO, "%!FUNC!(xdma=0x%p, request=0x%p, nBar=%d)", xdma, request, nBar);
    TraceVerbose(DBG_IO, "%!FUNC! writing %llu bytes at BAR %u at offset 0x%llx",
                 length, nBar, offset);

    status = ValidateBarParams(xdma, nBar, offset, length);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! ValidateBarParams failed: %!STATUS!", status);
        return status;
    }

    // Static Driver Verifier is not smart enough to see that length is checked in ValidateBarParams
    // Therefore we need to check it here as well
    if (length == 0) {
        TraceError(DBG_IO, "%!FUNC! failed: attempting to read 0 bytes");
        return STATUS_INVALID_DEVICE_REQUEST;
    }

    // calculate virtual address of the mmap'd BAR location
    writeAddr = (PUCHAR)xdma->bar[nBar] + offset;

    WDFMEMORY requestMemory;
    // get handle to the IO request memory which holds data to write
    status = WdfRequestRetrieveInputMemory(request, &requestMemory);
    if (!NT_SUCCESS(status)) {
        TraceInfo(DBG_IO, "%!FUNC! WdfRequestRetrieveInputMemory failed: %!STATUS!", status);
        return status;
    }

    // copy request memory to mmap'd BAR

	//*(unsigned int *)writeAddr = 0x01020304;
	
	unsigned int user_val = 0;
	if ((UINT32)xdma->userBarIdx == nBar)
	{
		status = WdfMemoryCopyToBuffer(requestMemory, 0, (PUCHAR)&user_val, 4);
		if (!NT_SUCCESS(status)) {
			TraceError(DBG_IO, "%!FUNC! WdfMemoryCopyToBuffer() failed : %!STATUS!", status);
			return status;
		}
		*(unsigned int *)writeAddr = user_val;
		status = STATUS_SUCCESS;
	}
	else
	{
		status = WdfMemoryCopyToBuffer(requestMemory, 0, writeAddr, length);
		if (!NT_SUCCESS(status)) {
			TraceError(DBG_IO, "%!FUNC! WdfMemoryCopyToBuffer() failed : %!STATUS!", status);
			return status;
		}
	}
	
    return status;
}

static NTSTATUS CopyDescriptorsToRequestMemory(IN XDMA_ENGINE* engine, WDFREQUEST request,
                                               size_t maximumLength, size_t* readBytes) {
    NTSTATUS status = STATUS_SUCCESS;
    WDFMEMORY requestMemory;

    TraceVerbose(DBG_IO, "%!FUNC!(engine=%p, request=%p, maximumLength=%llu, numBytes=%p)",
                 engine, request, maximumLength, readBytes);

    *readBytes = 0;

    if (maximumLength % sizeof(DMA_DESCRIPTOR) != 0) {
        TraceError(DBG_IO, "%!FUNC! max read length must be multiple of descriptor size (%u bytes)!",
                   sizeof(DMA_DESCRIPTOR));
        return STATUS_INVALID_PARAMETER;
    }

    DMA_DESCRIPTOR* descriptors = (DMA_DESCRIPTOR*)WdfCommonBufferGetAlignedVirtualAddress(engine->descBuffer);
    const size_t maxNumDesc = WdfCommonBufferGetLength(engine->descBuffer) * sizeof(DMA_DESCRIPTOR);

    size_t numDesc = 0;
    for (numDesc = 0; numDesc < maxNumDesc; numDesc++) {
        if (descriptors[numDesc].numBytes == 0) {
            break;
        }
    }

    size_t numBytes = (numDesc) * sizeof(DMA_DESCRIPTOR);

    if (numBytes > maximumLength) {
        TraceError(DBG_IO, "%!FUNC! user buffer not big enough! need at least %llu bytes", numBytes);
        return STATUS_INSUFFICIENT_RESOURCES;
    }

    if (numBytes == 0) {
        TraceWarning(DBG_IO, "%!FUNC! No valid descriptors found. No transfer pending?");
        return STATUS_SUCCESS;
    } else if (!(descriptors[numDesc - 1].control & (XDMA_DESC_STOP_BIT | XDMA_DESC_EOP_BIT))) {
        TraceWarning(DBG_IO, "%!FUNC! Couldn't find an End of Packet bit in the descriptors. Are they currently being written to?");
        return STATUS_SUCCESS;
    }

    TraceVerbose(DBG_IO, "%!FUNC! numDescriptors to copy = %llu", numBytes / sizeof(DMA_DESCRIPTOR));


    // get handle to the IO request memory which will hold the read data
    status = WdfRequestRetrieveOutputMemory(request, &requestMemory);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfRequestRetrieveOutputMemory failed: %!STATUS!", status);
        return status;
    }

    // copy from descriptor buffer into request memory
    status = WdfMemoryCopyFromBuffer(requestMemory, 0, descriptors, numBytes);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfMemoryCopyFromBuffer failed: %!STATUS!", status);
        return status;
    }

    *readBytes = numBytes;

    TraceVerbose(DBG_IO, "%!FUNC! numBytes=%llu, status=%!STATUS!", *readBytes, status);
    return status;
}

static NTSTATUS WriteBypassDescriptor(IN PXDMA_DEVICE xdma, IN WDFREQUEST request, IN XDMA_ENGINE* engine) {

    WDF_REQUEST_PARAMETERS params;
    WDF_REQUEST_PARAMETERS_INIT(&params);
    WdfRequestGetParameters(request, &params);
    size_t length = params.Parameters.Read.Length;

    NTSTATUS status = ValidateBarParams(xdma, xdma->bypassBarIdx, engine->bypassOffset, length);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! ValidateBarParams failed: %!STATUS!", status);
        return status;
    }

    if (length % sizeof(UINT32) != 0) {
        TraceError(DBG_IO, "%!FUNC! length is not multiple of 32bits!");
        return STATUS_INVALID_PARAMETER;
    }

    // get handle to the IO request memory which holds data to write
    WDFMEMORY requestMemory;
    status = WdfRequestRetrieveInputMemory(request, &requestMemory);
    if (!NT_SUCCESS(status)) {
        TraceInfo(DBG_IO, "%!FUNC! WdfRequestRetrieveInputMemory failed: %!STATUS!", status);
        return status;
    }

    // calculate virtual address of the mmap'd BAR location
    TraceInfo(DBG_IO, "%!FUNC! BAR offset=0x%04x", engine->bypassOffset);
    UINT32* writeAddr = (UINT32*)xdma->bar[xdma->bypassBarIdx] + (engine->bypassOffset / sizeof(UINT32));

    // send the descriptors to the bypass BAR in chunks of 32bits - all to the same address
    size_t requestOffset = 0;
    for (UINT i = 0; i < (length / sizeof(UINT32)); i++) {
        UINT32 descriptorChunk = 0;
        status = WdfMemoryCopyToBuffer(requestMemory, requestOffset, &descriptorChunk, sizeof(UINT32));
        if (!NT_SUCCESS(status)) {
            TraceError(DBG_IO, "%!FUNC! WdfMemoryCopyToBuffer() failed : %!STATUS!", status);
            return status;
        }
        TraceVerbose(DBG_IO, "%!FUNC! descBuffer[%u]=0x%08x", i, descriptorChunk);

        MemoryBarrier();
        *writeAddr = descriptorChunk;
        MemoryBarrier();
        requestOffset += sizeof(UINT32);
    }
    return status;
}

VOID EvtIoRead(IN WDFQUEUE queue, IN WDFREQUEST request, IN size_t length)
// Callback function on Device node ReadFile
{
    NTSTATUS status = STATUS_SUCCESS;
    PXDMA_DEVICE xdma = GetDeviceContext(WdfIoQueueGetDevice(queue));
    PFILE_CONTEXT file = GetFileContext(WdfRequestGetFileObject(request));

    UNREFERENCED_PARAMETER(length); // length can be extracted from request

    TraceInfo(DBG_IO, "%!FUNC! (Queue=%p, Request=%p, Length=%llu)", queue, request, length);
    //TraceInfo(DBG_IO, "%!FUNC! devNodeType %d", file->devType);

    switch (file->devType) {
    case DEVNODE_TYPE_USER:
        // handle request here without forwarding - read from PCIe BAR into request memory
        status = ReadBarToRequest(xdma, request, xdma->userBarIdx);
        if (NT_SUCCESS(status)) {
            // complete the request - read bytes are in the requestMemory
            WdfRequestCompleteWithInformation(request, status, length);
        }
        break;
    case DEVNODE_TYPE_CONTROL:
        // handle request here without forwarding - read from PCIe BAR into request memory
        status = ReadBarToRequest(xdma, request, xdma->configBarIdx);
        if (NT_SUCCESS(status)) {
            // complete the request - read bytes are in the requestMemory
            WdfRequestCompleteWithInformation(request, status, length);
        }
        break;
    case DEVNODE_TYPE_BYPASS:
        // handle request here without forwarding - read from PCIe BAR into request memory
        status = ReadBarToRequest(xdma, request, xdma->bypassBarIdx);
        if (NT_SUCCESS(status)) {
            // complete the request - read bytes are in the requestMemory
            WdfRequestCompleteWithInformation(request, status, length);
        }
        break;
    case DEVNODE_TYPE_BYPASS_H2C:
    case DEVNODE_TYPE_BYPASS_C2H:
    {
        ASSERTMSG("no read queue attached to this engine", file->queue != NULL);

        size_t readBytes = 0;
        status = CopyDescriptorsToRequestMemory(GetQueueContext(file->queue)->engine, request,
                                                length, &readBytes);
        if (NT_SUCCESS(status)) {
            // complete the request - read bytes are in the requestMemory
            WdfRequestCompleteWithInformation(request, status, readBytes);
        }
        break;
    }
    case DEVNODE_TYPE_EVENTS:
        ASSERTMSG("no write queue attached to this engine", file->queue != NULL);
        // forward request to engine queue - completed by EvtIoReadDma later
        WdfIoQueueStop(file->queue, NULL, NULL);
        status = WdfRequestForwardToIoQueue(request, file->queue);
        break;
    case DEVNODE_TYPE_C2H:
    {
        ASSERTMSG("no write queue attached to this engine", file->queue != NULL);

        XDMA_ENGINE* engine = GetQueueContext(file->queue)->engine;
        if (engine->streaming == TRUE) {
            if ((engine->ring.head == engine->ring.tail) && engine->ring.overflow == FALSE) {
                WdfIoQueueStop(file->queue, NULL, NULL);
            }
            if (engine->parentDevice->pollMode) {
                EnginePollRing(engine);
            }
        }
        // forward request to engine queue - completed by EvtIoReadDma later
        status = WdfRequestForwardToIoQueue(request, file->queue);
        break;
    }
    case DEVNODE_TYPE_H2C:
    default:
        //TraceError(DBG_IO, "%!FUNC! fails with invalid DevNodeID %d", file->devType);
        status = STATUS_INVALID_DEVICE_REQUEST;
    }

    if (!NT_SUCCESS(status)) {
        TraceInfo(DBG_IO, "%!FUNC! request complete with %!STATUS!", status);
        WdfRequestComplete(request, status);
    }

    return; // request has been either completed directly or forwarded to a queue
}

VOID EvtIoWrite(IN WDFQUEUE queue, IN WDFREQUEST request, IN size_t length)
// Callback function on Device node write operations
{
    NTSTATUS status = STATUS_SUCCESS;
    PXDMA_DEVICE xdma = GetDeviceContext(WdfIoQueueGetDevice(queue));
    PFILE_CONTEXT file = GetFileContext(WdfRequestGetFileObject(request));

    UNREFERENCED_PARAMETER(length); // length can be extracted from request

    TraceInfo(DBG_IO, "%!FUNC! (Queue=%p, Request=%p)", queue, request);
    //TraceInfo(DBG_IO, "%!FUNC! DevNodeID %d", file->devType);

    switch (file->devType) {
    case DEVNODE_TYPE_USER:
        // handle request here without forwarding. write to PCIe BAR from request memory
        status = WriteBarFromRequest(xdma, request, xdma->userBarIdx);
        if (NT_SUCCESS(status)) {
            WdfRequestCompleteWithInformation(request, status, length);  // complete the request        }
        }
        break;
    case DEVNODE_TYPE_CONTROL:
        // handle request here without forwarding. write to PCIe BAR from request memory
        status = WriteBarFromRequest(xdma, request, xdma->configBarIdx);
        if (NT_SUCCESS(status)) {
            WdfRequestCompleteWithInformation(request, status, length);  // complete the request        }
        }
        break;
    case DEVNODE_TYPE_BYPASS:
        // handle request here without forwarding. write to PCIe BAR from request memory
        status = WriteBarFromRequest(xdma, request, xdma->bypassBarIdx);
        if (NT_SUCCESS(status)) {
            WdfRequestCompleteWithInformation(request, status, length);  // complete the request        }
        }
        break;
    case DEVNODE_TYPE_BYPASS_H2C:
    case DEVNODE_TYPE_BYPASS_C2H:
        ASSERTMSG("no read queue attached to this engine", file->queue != NULL);

        // handle request here without forwarding. write to PCIe BAR from request memory
        status = WriteBypassDescriptor(xdma, request, GetQueueContext(file->queue)->engine);
        if (NT_SUCCESS(status)) {
            WdfRequestCompleteWithInformation(request, status, length);  // complete the request        }
        }
        break;
    case DEVNODE_TYPE_H2C:
        ASSERTMSG("no read queue attached to this engine", file->queue != NULL);

        // forward request to write engine queue, it ends up in EvtIoWriteDma() later
        status = WdfRequestForwardToIoQueue(request, file->queue);
        break;
    case DEVNODE_TYPE_C2H:
    default:
        status = STATUS_INVALID_DEVICE_REQUEST;
        break;
    }

    if (!NT_SUCCESS(status)) {
        WdfRequestComplete(request, status);
        TraceInfo(DBG_IO, "%!FUNC! failed Request 0x%p: %!STATUS!", request, status);
    }

    return; // request has been either completed directly or forwarded to a queue
}

NTSTATUS IoctlGetPerf(IN WDFREQUEST request, IN XDMA_ENGINE* engine, size_t* numBytes) {
    
    ASSERT(engine != NULL);
    ASSERT(numBytes != NULL);
    
    XDMA_PERF_DATA perfData = { 0 };
    EngineGetPerf(engine, &perfData);

    // get handle to the IO request memory which will hold the read data
    WDFMEMORY requestMemory;
    NTSTATUS status = WdfRequestRetrieveOutputMemory(request, &requestMemory);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfRequestRetrieveOutputMemory failed: %!STATUS!", status);
        return status;
    }

    // copy from perfData into request memory
    status = WdfMemoryCopyFromBuffer(requestMemory, 0, &perfData, sizeof(perfData));
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfMemoryCopyFromBuffer failed: %!STATUS!", status);
        return status;
    }
    *numBytes = sizeof(perfData);
    TraceVerbose(DBG_IO, "%!FUNC! exit with status: %!STATUS!", status);
    return status;
}

VOID EvtIoDeviceControl(IN WDFQUEUE Queue, IN WDFREQUEST request, IN size_t OutputBufferLength,
                        IN size_t InputBufferLength, IN ULONG IoControlCode) {
    
    UNREFERENCED_PARAMETER(Queue);
    UNREFERENCED_PARAMETER(OutputBufferLength);
    UNREFERENCED_PARAMETER(InputBufferLength);

    PFILE_CONTEXT file = GetFileContext(WdfRequestGetFileObject(request));
    PQUEUE_CONTEXT queue = GetQueueContext(file->queue);
    NTSTATUS status = STATUS_NOT_SUPPORTED;

    ASSERT(queue != NULL);

    // ioctl codes defined in xdma_public.h
    switch (IoControlCode) {
    case IOCTL_XDMA_PERF_START:
        TraceVerbose(DBG_IO, "%!FUNC! IOCTL_XDMA_PERF_START");
        if (queue->engine == NULL) {
            TraceError(DBG_IO, "%!FUNC! IOCTL only supported on DMA files (hc2_* or c2h_* devices)");
            status = STATUS_INVALID_PARAMETER;
            break;
        }
		DbgPrint("EvtIoDeviceControl EngineStartPerf\n");
        EngineStartPerf(queue->engine);
        WdfRequestComplete(request, STATUS_SUCCESS);
        return;
    case IOCTL_XDMA_PERF_GET:
    {
        TraceVerbose(DBG_IO, "%!FUNC! IOCTL_XDMA_PERF_GET");
        if (queue->engine == NULL) {
            TraceError(DBG_IO, "%!FUNC! IOCTL only supported on DMA files (hc2_* or c2h_* devices)");
            status = STATUS_INVALID_PARAMETER;
            break;
        }
        size_t numBytes = 0;
        status = IoctlGetPerf(request, queue->engine, &numBytes);
        if (!NT_SUCCESS(status)) {
            break;
        }
        // complete the request - read bytes are in the requestMemory
        WdfRequestCompleteWithInformation(request, status, numBytes);
        return;
    }
    default:
        TraceError(DBG_IO, "%!FUNC! Not implemented");
        status = STATUS_NOT_SUPPORTED;
        break;
    }

    WdfRequestComplete(request, status);
    TraceVerbose(DBG_IO, "%!FUNC! exit with status: %!STATUS!", status);
}

VOID ServiceUserEvent(IN WDFQUEUE Queue, IN WDFREQUEST Request, IN size_t Length)
// this callback function is executed when the queue is activated after a user interrupt happened, 
// and thus a read can complete immediately.
{
    PXDMA_DEVICE xdma = GetDeviceContext(WdfIoQueueGetDevice(Queue));
    FILE_CONTEXT* file = GetFileContext(WdfRequestGetFileObject(Request));
    WDFMEMORY outputMem;
    size_t bufSize = 0;
    NTSTATUS status;
    XDMA_EVENTS* userEvent;
    BOOLEAN eventValue = TRUE; // return a 1 to indicate that event occurred

    UNREFERENCED_PARAMETER(Length);

    userEvent = &xdma->userEvents[file->channel];
    TraceVerbose(DBG_USER, "%!FUNC!(Queue=0x%p, Request=0x%p, Length=%lld)", Queue, Request, Length);

    // get output buffer
    status = WdfRequestRetrieveOutputMemory(Request, &outputMem);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_USER, "%!FUNC! WdfRequestRetrieveOutputMemory failed: %!STATUS!", status);
        goto ErrExit;
    }

    // get buffer size
    WdfMemoryGetBuffer(outputMem, &bufSize);
    if (bufSize != sizeof(BOOLEAN)) {
        TraceError(DBG_USER, "%!FUNC! failed: length is %llu but must be %llu",
                   bufSize, sizeof(UINT32));
        status = STATUS_INVALID_PARAMETER;
        goto ErrExit;
    }

    // copy the user irq events from our internal buffer to the WdfMemory
    status = WdfMemoryCopyFromBuffer(outputMem, 0, &eventValue, bufSize);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_USER, "%!FUNC! WdfMemoryCopyFromBuffer failed: %!STATUS!", status);
        goto ErrExit;
    }

    WdfRequestCompleteWithInformation(Request, status, bufSize);
    TraceInfo(DBG_USER, "%!FUNC! user events returned is 0x%08X", eventValue);
    goto Exit;

ErrExit:
    WdfRequestComplete(Request, status);
    TraceInfo(DBG_USER, "%!FUNC! failed: %!STATUS!", status);
Exit:
    WdfIoQueueStop(userEvent->queue, NULL, NULL);

    TraceVerbose(DBG_USER, "%!FUNC! user EP=0x%08X", eventValue);
    return;
}
