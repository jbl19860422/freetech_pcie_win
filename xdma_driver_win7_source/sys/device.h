/*
* XDMA Device Driver
* ===============================
* 
* Copyright 2017 Xilinx Inc.
* Copyright 2010-2012 Sidebranch
* Copyright 2010-2012 Leon Woestenberg <leon@sidebranch.com>
*
* Maintainer:
* -----------
* Alexander Hornburg <alexande@xilinx.com>
*
* Description:
* ------------
* This is a sample driver for the Xilinx Inc. 'DMA/Bridge Subsystem for PCIe v3.0' (XDMA).
*
*
* References:
* -----------
*	[1] pg195-pcie-dma.pdf - DMA/Bridge Subsystem for PCI Express v3.0 - Product Guide
*/

#pragma once

// ========================= include dependencies =================================================

#include <ntddk.h>
#include "reg.h"
#include "dma_engine.h"

// ========================= constants ============================================================

#define XDMA_MAX_NUM_BARS (3)
#define XDMA_MAX_USER_IRQ (16)
#define XMDA_MAX_NUM_IRQ (XDMA_MAX_USER_IRQ + XDMA_MAX_CHAN_IRQ)

// ========================= type declarations ====================================================

// user event interrupt context
typedef struct XDMA_EVENTS_T {
    WDFQUEUE queue; // events read queue
    WDFINTERRUPT irq;
} XDMA_EVENTS;

// The device extension for the device object
typedef struct XDMA_DEVICE_T {

    // PCIe management
    BUS_INTERFACE_STANDARD pciBus;
    ULONG pciBusNr;
    ULONG pciDeviceNr;
    ULONG pciFunctionNr;
    PCI_COMMON_HEADER pciHeader;

    // PCIe BAR access
    UINT numBars;
    BOOLEAN barIsMemory[XDMA_MAX_NUM_BARS];
    PVOID bar[XDMA_MAX_NUM_BARS]; // kernel virtual address of BAR
    ULONG barLength[XDMA_MAX_NUM_BARS];
    ULONG configBarIdx;
    LONG userBarIdx;
    LONG bypassBarIdx;
    XDMA_CONFIG_REGS *configRegs;
    XDMA_IRQ_REGS *interruptRegs;

    // DMA Engine management
    XDMA_ENGINE engines[XDMA_MAX_NUM_CHANNELS][XDMA_NUM_DIRECTIONS];
    WDFDMAENABLER dmaEnabler; // WDF DMA Enabler for the engine queues

    // Interrupt Resources
    ULONG numIrqResources;
    WDFINTERRUPT lineInterrupt;
    WDFINTERRUPT channelInterrupts[XDMA_MAX_CHAN_IRQ];

    // user events
    XDMA_EVENTS userEvents[XDMA_MAX_USER_IRQ];

    // queue related
    WDFQUEUE entryQueue; // entry queue, all request start here
    WDFQUEUE eventsReadQueue; // events read queue

    ULONG pollMode;

} XDMA_DEVICE, *PXDMA_DEVICE;
WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(XDMA_DEVICE, GetDeviceContext)

// ========================= function declarations ================================================

EVT_WDF_DRIVER_DEVICE_ADD           EvtDeviceAdd;
EVT_WDF_DEVICE_CONTEXT_CLEANUP      EvtDeviceCleanup;
EVT_WDF_DEVICE_PREPARE_HARDWARE     EvtDevicePrepareHardware;
EVT_WDF_DEVICE_RELEASE_HARDWARE     EvtDeviceReleaseHardware;

