/*
* XDMA Interrupt Service Routines, Handlers and Dispatch Functions
* ===============================
* 
* Copyright 2017 Xilinx Inc.
* Copyright 2010-2012 Sidebranch
* Copyright 2010-2012 Leon Woestenberg <leon@sidebranch.com>
*
* Maintainer:
* -----------
* Alexander Hornburg <alexande@xilinx.com>
*
*/

#pragma once

// ========================= include dependencies =================================================
#include <ntddk.h>
#include "dma_engine.h"

// ========================= declarations =================================================

typedef struct _IRQ_CONTEXT {
    ULONG eventId;
    UINT32 channelIrqPending; // channel irq that have fired
    UINT32 userIrqPending; // user event irq that have fired
    XDMA_ENGINE *engine;
} IRQ_CONTEXT, *PIRQ_CONTEXT;
WDF_DECLARE_CONTEXT_TYPE_WITH_NAME(IRQ_CONTEXT, GetIrqContext)

EVT_WDF_INTERRUPT_ISR               EvtInterruptIsr;
EVT_WDF_INTERRUPT_DPC               EvtInterruptDpc;
EVT_WDF_INTERRUPT_ENABLE            EvtInterruptEnable;
EVT_WDF_INTERRUPT_DISABLE           EvtInterruptDisable;

EVT_WDF_INTERRUPT_ISR               EvtChannelInterruptIsr;
EVT_WDF_INTERRUPT_DPC               EvtChannelInterruptDpc;
EVT_WDF_INTERRUPT_ENABLE            EvtChannelInterruptEnable;
EVT_WDF_INTERRUPT_DISABLE           EvtChannelInterruptDisable;

EVT_WDF_INTERRUPT_ISR               EvtUserInterruptIsr;
EVT_WDF_INTERRUPT_DPC               EvtUserInterruptDpc;
EVT_WDF_INTERRUPT_ENABLE            EvtUserInterruptEnable;
EVT_WDF_INTERRUPT_DISABLE           EvtUserInterruptDisable;

NTSTATUS SetupSingleInterruptResource(IN WDFDEVICE device, IN WDFCMRESLIST ResourcesRaw,
                              IN WDFCMRESLIST ResourcesTranslated);

NTSTATUS SetupMultipleInterruptResources(IN WDFDEVICE device, IN WDFCMRESLIST ResourcesRaw,
                                 IN WDFCMRESLIST ResourcesTranslated);

