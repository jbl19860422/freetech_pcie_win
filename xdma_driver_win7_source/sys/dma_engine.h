/*
* XDMA Scatter-Gather DMA Engines
* ===============================
* 
* Copyright 2017 Xilinx Inc.
* Copyright 2010-2012 Sidebranch
* Copyright 2010-2012 Leon Woestenberg <leon@sidebranch.com>
*
* Maintainer:
* -----------
* Alexander Hornburg <alexande@xilinx.com>
*
*/

#pragma once

// ========================= include dependencies =================================================

#include <ntddk.h>
#include <ntintsafe.h>
#include <wdf.h>
#include "reg.h"
#include "xdma_public.h"

// ========================= constants =================================================

#define XDMA_MAX_NUM_CHANNELS   (4)
#define XDMA_NUM_DIRECTIONS     (2)
#define XDMA_MAX_CHAN_IRQ       (XDMA_NUM_DIRECTIONS * XDMA_MAX_NUM_CHANNELS)
#define XDMA_RING_NUM_BLOCKS    (258U)
#define XDMA_RING_BLOCK_SIZE    (PAGE_SIZE)
#define XDMA_ENG_IRQ_NUM        (1)
#define MAX_TRANSFER_SIZE       (8UL * 1024UL * 1024UL)
#define XDMA_DESC_MAGIC         (0xAD4B0000)
#define XDMA_WB_COUNT_MASK      (0x00ffffffUL)
#define XDMA_WB_ERR_MASK        (BIT_N(31))

// Direction of the DMA transfer/engine
typedef enum DirToDev_t {
    H2C = 0, // Host-to-Card - write to device
    C2H = 1  // Card-to-Host - read from device
} DirToDev;

// ========================= type declarations =================================================

//forward declarations
struct XDMA_DEVICE_T; 
typedef struct XDMA_DEVICE_T XDMA_DEVICE;
struct XDMA_ENGINE_T;

// Ring buffer abstraction for streaming DMA
typedef struct XDMA_RING_T {
    WDFCOMMONBUFFER results;
    PMDL mdl[XDMA_RING_NUM_BLOCKS]; // memory descriptor list - host side
    CHAR dmaTransferContext[DMA_TRANSFER_CONTEXT_SIZE_V1];
    UINT head;
    UINT tail;
    BOOLEAN overflow;
    WDFSPINLOCK lock;
}XDMA_RING, *PXDMA_RING;

// engine specific work to perform after dma transfer completion is detected
typedef VOID(*PFN_XDMA_ENGINE_WORK)(IN struct XDMA_ENGINE_T *engine);

// DMA engine abstraction
typedef struct XDMA_ENGINE_T {

    XDMA_DEVICE *parentDevice; // the xdma device to which this engine belongs

    // register access
    XDMA_ENGINE_REGS *regs; // control regs
    XDMA_SGDMA_REGS *sgdma;

    // engine configuration
    UINT32 irqBitMask;
    UINT32 alignAddr;
    UINT32 alignLength;
    UINT32 alignAddrBits;
    DWORD channel;
    DirToDev dir; 	// type of engine H2C or C2H
    BOOLEAN enabled;
    BOOLEAN streaming;

    // dma transfer related
    WDFCOMMONBUFFER descBuffer;
    WDFDMATRANSACTION dmaTransaction;
    WDFQUEUE queue;
    PFN_XDMA_ENGINE_WORK work; // engine work for interrupt processing

    // specific to streaming interface
    XDMA_RING ring;

    // specific to descriptor bypass feature
    ULONG bypassOffset;

    // specific to poll mode
    WDFCOMMONBUFFER pollWbBuffer; // buffer for holding poll mode descriptor writeback data
    ULONG numDescriptors; // keep count of descriptors in transfer for poll mode

    ULONGLONG lastInterruptMs;
} XDMA_ENGINE;

#pragma pack(1)

// Descriptor for a single contiguous memory block transfer.
//
// Multiple descriptors are linked by means of the next pointer. An additional
// extra adjacent number gives the amount of extra contiguous descriptors.
// The descriptors are in root complex memory, and the bytes in the 32-bit
// words must be in little-endian byte ordering.
typedef struct {
    UINT32 control;
    UINT32 numBytes;  // transfer length in bytes
    UINT32 srcAddrLo; // source address (low 32-bit)
    UINT32 srcAddrHi; // source address (high 32-bit)
    UINT32 dstAddrLo; // destination address (low 32-bit)
    UINT32 dstAddrHi; // destination address (high 32-bit)
                      // next descriptor in the single-linked list of descriptors, 
                      // this is the bus address of the next descriptor in the root complex memory.
    UINT32 nextLo;    // next desc address (low 32-bit)
    UINT32 nextHi;    // next desc address (high 32-bit)
} DMA_DESCRIPTOR;

// Result buffer of the streaming DMA operation. 
// The XDMA IP core writes the result of the DMA transfer to the host memory
typedef struct {
    UINT32 status;
    UINT32 length;
    UINT32 reserved_1[6]; // padding
} DMA_RESULT;

// Structure for polled mode descriptor writeback
// XDMA IP core writes number of completed descriptors to this memory, which the driver can then
// poll to detect transfer completion
typedef struct {
    UINT32 completedDescCount;
    UINT32 reserved_1[7];
} XDMA_POLL_WB;

#pragma pack()

// ========================= function declarations =================================================

// Callback for when a C2H DMA transfer completes
// Completes the IO request from EvtIoRead()
EVT_WDF_IO_QUEUE_IO_READ    EvtIoReadDma;

// Callback for when a H2C DMA transfer completes
// Completes the IO request from EvtIoWrite()
EVT_WDF_IO_QUEUE_IO_WRITE   EvtIoWriteDma;

// Completes the IO request from EvtIoRead() for streaming engines
EVT_WDF_IO_QUEUE_IO_READ    EvtIoReadEngineRing;

// Initialize an XDMA_ENGINE for each engine configured in HW
NTSTATUS ProbeEngines(IN WDFDEVICE device, IN PUCHAR configBarAddr);

// Get the engine status register from HW - optionally clear the register  when 'clear'=TRUE
UINT32 EngineStatus(IN XDMA_ENGINE *engine, IN BOOLEAN clear);

// Get DMA transfer alignment requirements (address granularity, transfer length and address bits)
VOID EngineGetAlignments(IN OUT XDMA_ENGINE *engine);

// Start the DMA engine
// The transfer descriptors should be initialized and bound to HW before calling this function
VOID EngineStart(IN XDMA_ENGINE *engine);

// Stop the DMA engine
VOID EngineStop(IN XDMA_ENGINE *engine);

// Configure the streaming ring buffer and start the cyclic DMA transfer
VOID EngineRingSetup(IN XDMA_ENGINE *engine);

// Reset the streaming ring buffer and stop the cyclic DMA transfer
VOID EngineRingTeardown(IN XDMA_ENGINE *engine);

// Poll the write-back buffer for DMA transfer completion
NTSTATUS EnginePollTransfer(IN XDMA_ENGINE* engine);

// Poll the write-back buffer for DMA transfer completion
NTSTATUS EnginePollRing(IN XDMA_ENGINE* engine);

// enable the engines interrupt
VOID EngineEnableInterrupt(IN XDMA_ENGINE* engine);

// disable the engines interrupt
VOID EngineDisableInterrupt(IN XDMA_ENGINE* engine);

// Arm the performance counters of the XDMA engine
// Automatically stops when a dma descriptor with the stop bit is completed
VOID EngineStartPerf(IN XDMA_ENGINE* engine);

// Get the performance counters 
VOID EngineGetPerf(IN XDMA_ENGINE* engine, OUT XDMA_PERF_DATA* perfData);
