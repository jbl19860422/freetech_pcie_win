/*
* XDMA Scatter-Gather DMA Engines
* ===============================
* 
* Copyright 2017 Xilinx Inc.
* Copyright 2010-2012 Sidebranch
* Copyright 2010-2012 Leon Woestenberg <leon@sidebranch.com>
*
* Maintainer:
* -----------
* Alexander Hornburg <alexande@xilinx.com>
*
* References:
* -----------
*	[1] pg195-pcie-dma.pdf - DMA/Bridge Subsystem for PCI Express v3.0 - Product Guide
*/

// ========================= include dependencies =================================================

#include "device.h"
#include "file_io.h"
#include "interrupt.h"
#include "dma_engine.h"
#include "trace.h"

#ifdef DBG
// The trace message header (.tmh) file must be included in a source file before any WPP macro 
// calls and after defining a WPP_CONTROL_GUIDS macro (defined in trace.h). see trace.h
#include "dma_engine.tmh"
#endif

// ========================= static function declarations =========================================

static NTSTATUS EngineCreateQueue(IN WDFDEVICE device, IN OUT XDMA_ENGINE* engine);
static NTSTATUS EngineCreateDescriptorBuffer(IN OUT XDMA_ENGINE *engine);
static NTSTATUS EngineCreateRingBuffer(IN XDMA_ENGINE* engine, IN WDFOBJECT device);
static NTSTATUS EngineCreateResultBuffer(IN XDMA_ENGINE* engine, IN WDFOBJECT device);
static VOID EngineConfigureInterrupt(IN OUT XDMA_ENGINE *engine, IN UINT index);
static VOID EngineProcessTransfer(IN XDMA_ENGINE *engine);
static UINT EngineProcessRing(IN XDMA_ENGINE *engine);
static NTSTATUS EngineCreatePollWriteBackBuffer(IN OUT XDMA_ENGINE *engine);

EVT_WDF_PROGRAM_DMA EvtProgramDma;
EVT_WDF_REQUEST_CANCEL EvtRequestCancel;

// Mark these functions as pageable code
#ifdef ALLOC_PRAGMA
#pragma alloc_text (PAGE, EngineCreateQueue)
#pragma alloc_text (PAGE, ProbeEngines)
#endif

// WDK 10 static code analysis gives a false warning: "Allocating executable memory via specifying 
// a MM_PAGE_PRIORITY type without a bitwise OR with MdlMappingNoExecute"
// However this only applies to Windows 8 and above, thus disable this warning. 
// see https://msdn.microsoft.com/en-us/library/windows/hardware/ff554629(v=vs.85).aspx
#pragma warning (disable : 30030) 

// ======================== common engine functions ===============================================

static NTSTATUS EngineCreateQueue(IN WDFDEVICE device, IN OUT XDMA_ENGINE* engine)
// Create a WDF IO queue for a DMA engine
{
    NTSTATUS status = STATUS_SUCCESS;
    WDF_IO_QUEUE_CONFIG config;
    WDF_OBJECT_ATTRIBUTES attribs;
    PQUEUE_CONTEXT context;

    PAGED_CODE();

    // engine queue is sequential
    WDF_IO_QUEUE_CONFIG_INIT(&config, WdfIoQueueDispatchSequential);

    ASSERTMSG("direction is neither H2C nor C2H!", (engine->dir == C2H) || (engine->dir == H2C));
    if (engine->dir == H2C) { // callback handler for write requests
        config.EvtIoWrite = EvtIoWriteDma;
        TraceInfo(DBG_INIT, "%!FUNC! EvtIoWrite=EvtIoWriteDma");
    }
    else if (engine->dir == C2H) { // callback handler for read requests

        if (engine->streaming == TRUE) {
            config.EvtIoRead = EvtIoReadEngineRing;
            TraceInfo(DBG_INIT, "%!FUNC! EvtIoRead=EvtIoReadDmaCyclic");
        }
        else {
            config.EvtIoRead = EvtIoReadDma;
            TraceInfo(DBG_INIT, "%!FUNC! EvtIoRead=EvtIoReadDma");
        }
    }

    // serialize all callbacks related to this queue. see ref [2]
    WDF_OBJECT_ATTRIBUTES_INIT(&attribs);
    attribs.SynchronizationScope = WdfSynchronizationScopeQueue;
    WDF_OBJECT_ATTRIBUTES_SET_CONTEXT_TYPE(&attribs, QUEUE_CONTEXT);
    status = WdfIoQueueCreate(device, &config, &attribs, &(engine->queue));
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfIoQueueCreate failed %d", status);
        return status;
    }

    if ((engine->dir == C2H) && (engine->streaming == TRUE)) {
        // this queue should not forward requests until we ask it to
        WdfIoQueueStop(engine->queue, NULL, NULL);
    }

    // store arguments into queue context
    context = GetQueueContext(engine->queue);
    context->engine = engine;

    return status;
}

static NTSTATUS EngineCreateDescriptorBuffer(IN OUT XDMA_ENGINE *engine) {
    // allocate host-side buffer for descriptors
    SIZE_T bufferSize = (MAX_TRANSFER_SIZE / PAGE_SIZE + 2) * sizeof(DMA_DESCRIPTOR);

    NTSTATUS status = WdfCommonBufferCreate(engine->parentDevice->dmaEnabler, bufferSize, 
                                            WDF_NO_OBJECT_ATTRIBUTES, &engine->descBuffer);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfCommonBufferCreate failed: %!STATUS!", status);
        return status;
    }

    PHYSICAL_ADDRESS descBufferLA = WdfCommonBufferGetAlignedLogicalAddress(engine->descBuffer);
    PUCHAR descBufferVA = (PUCHAR)WdfCommonBufferGetAlignedVirtualAddress(engine->descBuffer);
    RtlZeroMemory(descBufferVA, bufferSize);

    // give hw the physical start address of the descriptor buffer
    engine->sgdma->firstDescLo = descBufferLA.LowPart;
    engine->sgdma->firstDescHi = descBufferLA.HighPart;
    engine->sgdma->firstDescAdj = 0;

    TraceInfo(DBG_INIT, "%!FUNC! descriptor buffer at 0x%08x%08x, size=%lld",
              engine->sgdma->firstDescHi, engine->sgdma->firstDescLo, bufferSize);
    return status;
}

static VOID EngineConfigureInterrupt(IN OUT XDMA_ENGINE *engine, IN UINT index) {
    // engine interrupt request bit(s) - interrupt bit depends on number of engines present
    // see Figure 2-4 on page 46 of pcie dma product guide [1]
    engine->irqBitMask = (1 << XDMA_ENG_IRQ_NUM) - 1;
    engine->irqBitMask <<= (index * XDMA_ENG_IRQ_NUM);

    // bind msi interrupt context with this engine
    if (engine->parentDevice->channelInterrupts[index] != NULL) {
        PIRQ_CONTEXT irqContext = GetIrqContext(engine->parentDevice->channelInterrupts[index]);
        irqContext->engine = engine;
    }

    // enable interrupts
    UINT32 regVal = XDMA_CTRL_IE_ALL;
    if (engine->streaming && (engine->dir == C2H)) {
        regVal |= XDMA_CTRL_IE_IDLE_STOPPED;
    }

    engine->regs->intEnableMaskW1S = regVal;
    engine->regs->controlW1S = regVal;
    TraceVerbose(DBG_INIT, "%!FUNC! engineIrqBitMask=0x%08x, intEnableMask=0x%08x",
                 engine->irqBitMask, engine->regs->intEnableMask);
}

static VOID EngineProcessTransfer(IN XDMA_ENGINE *engine)
// service an SGDMA engine
{
    NTSTATUS status = STATUS_SUCCESS;
    WDFREQUEST request;
    UINT32 engineStatus;

    if (engine == NULL) {
        TraceError(DBG_DPC, "%!FUNC! engine=NULL");
		DbgPrint(("engine=NULL\n"));
        return;
    }

    request = WdfDmaTransactionGetRequest(engine->dmaTransaction);
    if (!request) {
        TraceInfo(DBG_DPC, "Interrupt but no request pending?");
		DbgPrint("Interrupt but no request pending?, channel=%d\n", engine->channel);
        return;
    }

    // read and clear engine status 
    engineStatus = EngineStatus(engine, TRUE);
    TraceInfo(DBG_DPC, "%!FUNC! %s Engine Status: 0x%08x",
              engine->dir == H2C ? "H2C" : "C2H", engineStatus);

	DbgPrint("%s Engine Status: 0x%08x",
		engine->dir == H2C ? "H2C" : "C2H", engineStatus);

    EngineStop(engine);

    switch (engineStatus & XDMA_STAT_EXPECTED_ZERO) {
    case XDMA_ENGINE_STOPPED_OK: // engine not busy and no errors?
    {
        BOOLEAN completed = WdfDmaTransactionDmaCompleted(engine->dmaTransaction, &status);
        size_t bytesTransferred = WdfDmaTransactionGetBytesTransferred(engine->dmaTransaction);

        TraceInfo(DBG_DPC, "%!FUNC! transaction%scomplete, bytesTransferred=%llu",
                  completed ? " " : " in", bytesTransferred);


		DbgPrint("transaction %s complete, bytesTransferred=%llu",
			completed ? " " : " in", bytesTransferred);
        if (completed) {
            status = WdfRequestUnmarkCancelable(request);
            if (!NT_SUCCESS(status)) {
                TraceError(DBG_DPC, "%!FUNC! WdfRequestUnmarkCancelable failed: %!STATUS!", status);
				DbgPrint(("WdfRequestUnmarkCancelable failed:\n"));
            }
            status = WdfDmaTransactionRelease(engine->dmaTransaction);
            if (!NT_SUCCESS(status)) {
                TraceError(DBG_DPC, "%!FUNC! WdfDmaTransactionRelease failed: %!STATUS!", status);
				DbgPrint(("WdfDmaTransactionRelease failed: \n"));
            }
            WdfRequestCompleteWithInformation(request, status, bytesTransferred);
        }
        break;
    }
    case XDMA_BUSY_BIT: // engine is still busy without sign of errors?
        TraceError(DBG_DPC, "%!FUNC! Engine Still Busy, Descriptors Completed=%u\n",
                   engine->regs->completedDescCount);
		DbgPrint("Engine Still Busy, Descriptors Completed=%u\n",
			engine->regs->completedDescCount);
    default: // any sign of errors
        TraceError(DBG_DPC, "%!FUNC! Unexpected engine status 0x%08x", engineStatus);
		DbgPrint("Unexpected engine status 0x%08x", engineStatus);
        status = WdfRequestUnmarkCancelable(request);
        if (!NT_SUCCESS(status)) {
            TraceError(DBG_DPC, "%!FUNC! WdfRequestUnmarkCancelable failed: %!STATUS!", status);
			DbgPrint(("WdfRequestUnmarkCancelable failed: \n"));
        }
        status = WdfDmaTransactionRelease(engine->dmaTransaction);
        if (!NT_SUCCESS(status)) {
            TraceError(DBG_DPC, "%!FUNC! WdfDmaTransactionRelease failed: %!STATUS!", status);
			DbgPrint(("WdfDmaTransactionRelease failed: \n"));
        }
        WdfRequestComplete(request, STATUS_INTERNAL_ERROR);
    }

    // clear descriptor buffer
    DMA_DESCRIPTOR* descriptorBuffer = (DMA_DESCRIPTOR*)WdfCommonBufferGetAlignedVirtualAddress(engine->descBuffer);
    size_t descBufferLength = WdfCommonBufferGetLength(engine->descBuffer);
    RtlZeroMemory(descriptorBuffer, descBufferLength);

    // clear poll writeback buffer
    if (engine->parentDevice->pollMode) {
        XDMA_POLL_WB* wbBuffer = (XDMA_POLL_WB*)WdfCommonBufferGetAlignedVirtualAddress(engine->pollWbBuffer);
        size_t wbBufferLength = WdfCommonBufferGetLength(engine->pollWbBuffer);
        RtlZeroMemory(wbBuffer, wbBufferLength);
    }
}

static VOID DumpDescriptor(IN DMA_DESCRIPTOR* desc) {
#if DBG
    TraceInfo(DBG_DESC, "descriptor={.control=0x%08X, .numBytes=%u, srcAddr=0x%08X%08X, .dstAddr=0x%08X%08X, nextAddr=0x%08X%08X}",
              desc->control, desc->numBytes,
              desc->srcAddrHi, desc->srcAddrLo,
              desc->dstAddrHi, desc->dstAddrLo,
              desc->nextHi, desc->nextLo);
#else 
    UNREFERENCED_PARAMETER(desc);
#endif
}

static BOOLEAN DescriptorIsAligned(IN XDMA_ENGINE *engine, IN DMA_DESCRIPTOR *desc)
// 
{
    BOOLEAN dstAddrMisaligned = (desc->dstAddrLo % engine->alignAddr) != 0;
    BOOLEAN lengthMisaligned = (desc->numBytes % engine->alignLength) != 0;
    BOOLEAN srcAddrMisaligned = (desc->srcAddrLo % engine->alignAddr) != 0;

    if (dstAddrMisaligned || lengthMisaligned || srcAddrMisaligned) {
        TraceVerbose(DBG_DESC, "%!FUNC! descriptor alignments: src=0x%08llx, dst=0x%08llx, bytes=%llu",
                     desc->srcAddrLo, desc->dstAddrLo, desc->numBytes);
        TraceError(DBG_DESC, "%!FUNC! misalignment detected! src?=%u dst?=%u length?=%u",
                   srcAddrMisaligned, dstAddrMisaligned, lengthMisaligned);
        return FALSE;
    }
    return TRUE;
}

BOOLEAN EvtProgramDma(IN WDFDMATRANSACTION Transaction, IN WDFDEVICE Device,
                      IN WDFCONTEXT context, IN WDF_DMA_DIRECTION Direction,
                      IN PSCATTER_GATHER_LIST SgList)
    // this programs the engine to start a dma transfer
{
    WDFREQUEST request;
    WDF_REQUEST_PARAMETERS params;
    LONGLONG deviceOffset;
    LONGLONG splitOffset;
    DMA_DESCRIPTOR *descriptor;
    PHYSICAL_ADDRESS descBufferLA;
    XDMA_ENGINE * engine = (XDMA_ENGINE*)context;

    UNREFERENCED_PARAMETER(Device);
    request = WdfDmaTransactionGetRequest(Transaction);

    WDF_REQUEST_PARAMETERS_INIT(&params);
    WdfRequestGetParameters(request, &params);
    if (Direction == WdfDmaDirectionWriteToDevice)
        deviceOffset = (SIZE_T)params.Parameters.Write.DeviceOffset;
    else
        deviceOffset = (SIZE_T)params.Parameters.Read.DeviceOffset;

    // get virtual and physical pointers to descriptor buffer
    descriptor = (DMA_DESCRIPTOR*)WdfCommonBufferGetAlignedVirtualAddress(engine->descBuffer);
    descBufferLA = WdfCommonBufferGetAlignedLogicalAddress(engine->descBuffer);

    // offset into the transaction (if it is split)
    splitOffset = WdfDmaTransactionGetBytesTransferred(Transaction);
    deviceOffset += splitOffset;
    TraceInfo(DBG_DMA, "%!FUNC! device addr=%lld, num descriptors=%d",
              deviceOffset, SgList->NumberOfElements);

	PHYSICAL_ADDRESS resultBufferLA = WdfCommonBufferGetAlignedLogicalAddress(engine->ring.results);

    for (ULONG i = 0; i < SgList->NumberOfElements; i++) {
		descriptor[i].control = XDMA_DESC_MAGIC | XDMA_DESC_EOP_BIT;
        descriptor[i].numBytes = SgList->Elements[i].Length;
        ULONG hostAddrLo = SgList->Elements[i].Address.LowPart;
        LONG hostAddrHi = SgList->Elements[i].Address.HighPart;
        if (Direction == WdfDmaDirectionWriteToDevice) {
            // source is host memory
            descriptor[i].srcAddrLo = hostAddrLo;
            descriptor[i].srcAddrHi = hostAddrHi;
            /*descriptor[i].dstAddrLo = LIMIT_TO_32(deviceOffset);
            descriptor[i].dstAddrHi = LIMIT_TO_32(deviceOffset >> 32);*/
			descriptor[i].dstAddrLo = resultBufferLA.LowPart;
			descriptor[i].dstAddrHi = resultBufferLA.HighPart;
			resultBufferLA.QuadPart += sizeof(DMA_RESULT);
        } else {
            // destination is host memory
            /*descriptor[i].srcAddrLo = LIMIT_TO_32(deviceOffset);
            descriptor[i].srcAddrHi = LIMIT_TO_32(deviceOffset >> 32);*/
            descriptor[i].dstAddrLo = hostAddrLo;
            descriptor[i].dstAddrHi = hostAddrHi;
			descriptor[i].srcAddrLo = resultBufferLA.LowPart;
			descriptor[i].srcAddrHi = resultBufferLA.HighPart;
			resultBufferLA.QuadPart += sizeof(DMA_RESULT);
        }

        // next descriptor bus address 
        descBufferLA.QuadPart += sizeof(DMA_DESCRIPTOR);

        // non-last descriptor(s)? 
        if ((i + 1) < SgList->NumberOfElements) {
            descriptor[i].nextLo = descBufferLA.LowPart;
            descriptor[i].nextHi = descBufferLA.HighPart;
        } else { // last descriptor
                 // stop engine and request an interrupt from the engine
            descriptor[i].control |= (XDMA_DESC_STOP_BIT | XDMA_DESC_COMPLETED_BIT);
            //if (engine->streaming == TRUE) {
                descriptor[i].control |= XDMA_DESC_EOP_BIT;
                TraceVerbose(DBG_DMA, "%!FUNC! descriptor[i].control=0x%08x", descriptor[i].control);
            //}
            descriptor[i].nextLo = 0;
            descriptor[i].nextHi = 0;
        }
        deviceOffset += SgList->Elements[i].Length;

        if (FALSE == DescriptorIsAligned(engine, &(descriptor[i]))) {
            TraceWarning(DBG_DMA, "%!FUNC! failed: Dma Transfer is not aligned");
			DbgPrint(("failed: Dma Transfer is not aligned\n"));
        }

        DumpDescriptor(&(descriptor[i]));
    }

    if (engine->parentDevice->pollMode) {
        engine->numDescriptors = SgList->NumberOfElements;
    }

    MemoryBarrier();
	DbgPrint("EvtProgramDma EngineStart\n");
    // start the engine
    EngineStart(engine);

    MemoryBarrier();

    return TRUE;
}

NTSTATUS ProbeEngines(IN WDFDEVICE device, IN PUCHAR configBarAddr) {
    PAGED_CODE();

    PXDMA_DEVICE xdma = GetDeviceContext(device);
    ULONG engineIndex = 0;
    NTSTATUS status;

    // iterate over H2C (FPGA performs PCIe reads towards FPGA),
    // then C2H (FPGA performs PCIe writes from FPGA)
    for (UINT dir = H2C; dir < 2; dir++) { // 0=H2C, 1=C2H
        for (ULONG channel = 0; channel < XDMA_MAX_NUM_CHANNELS; channel++) {
            XDMA_ENGINE* engine = &(xdma->engines[channel][dir]);
            engine->enabled = FALSE;
            ULONG offset = (dir * BLOCK_OFFSET) + (channel * ENGINE_OFFSET);
            engine->regs = (XDMA_ENGINE_REGS*)(configBarAddr + offset);

            // skip inactive engines
            UINT32 engineID = engine->regs->identifier;
            if ((engineID & XDMA_ID_MASK) != XDMA_ID) {
                TraceWarning(DBG_INIT, "%!FUNC! Skipping unknown engine id 0x%08x", engineID);
                continue;
            }

            engine->parentDevice = xdma;
            engine->channel = channel;
            engine->dir = (DirToDev)dir;
            engine->sgdma = (XDMA_SGDMA_REGS*)(configBarAddr + offset + SGDMA_BLOCK_OFFSET);

            // is AXI-ST type engine?
            engine->streaming = (engineID & XDMA_ID_ST_BIT) > 0;
			engine->streaming = FALSE;
            // set interrupt sources
            if (!engine->parentDevice->pollMode) {
                EngineConfigureInterrupt(engine, engineIndex);
            } else {
                // create common buffer for poll mode descriptor write back
                status = EngineCreatePollWriteBackBuffer(engine);
                if (!NT_SUCCESS(status)) {
                    TraceError(DBG_INIT, "%!FUNC! EngineCreatePollWriteBackBuffer() failed: %!STATUS!",
                               status);
                    return status;
                }
            }
            // capture alignment requirements
            EngineGetAlignments(engine);

            // create the read queue for the write engine
            status = EngineCreateQueue(device, engine);
            if (!NT_SUCCESS(status)) {
                TraceError(DBG_INIT, "%!FUNC! EngineCreateQueue() failed: %!STATUS!", status);
                return status;
            }

            // create and bind dma desciptor buffer to hw
            status = EngineCreateDescriptorBuffer(engine);
            if (!NT_SUCCESS(status)) {
                TraceError(DBG_INIT, "%!FUNC! EngineCreateDescriptorBuffer() failed: %!STATUS!",
                           status);
                return status;
            }

            // allocate wdf dma transaction object
            status = WdfDmaTransactionCreate(xdma->dmaEnabler, WDF_NO_OBJECT_ATTRIBUTES,
                                             &engine->dmaTransaction);
            if (!NT_SUCCESS(status)) {
                TraceError(DBG_INIT, "WdfDmaTransactionCreate() failed: %!STATUS!", status);
                return status;
            }

			/*WdfDmaTransactionSetMaximumLength(engine->dmaTransaction, 4096);*/

            if ((engine->streaming == TRUE) && (engine->dir == C2H)) {
                engine->work = EngineProcessRing;
                status = EngineCreateRingBuffer(engine, device);
                if (!NT_SUCCESS(status)) {
                    TraceError(DBG_INIT, "EngineCreateStreamBuffers() failed: %!STATUS!", status);
                    return status;
                }
            }
            else {
				status = EngineCreateResultBuffer(engine, device);
                engine->work = EngineProcessTransfer;
            }

            engine->bypassOffset = engineIndex * XDMA_BYPASS_SPACING;

            engine->enabled = TRUE;
            engineIndex++;
            TraceInfo(DBG_INIT, "%!FUNC! created %s AXI-%s engine %d",
                      dir == C2H ? "C2H" : "H2C", engine->streaming ? "ST" : "MM", channel);
        }
    }
    return STATUS_SUCCESS;
}

VOID EngineGetAlignments(IN OUT XDMA_ENGINE *engine) {

    UINT32 alignments = engine->regs->alignments;
    UINT32 align_bytes = (alignments & 0x00ff0000U) >> 16;
    UINT32 granularity_bytes = (alignments & 0x0000ff00U) >> 8;
    UINT32 address_bits = (alignments & 0x000000ffU);

    if (alignments) {
        engine->alignAddr = align_bytes;
        engine->alignLength = granularity_bytes;
        engine->alignAddrBits = address_bits;
    }
    else { // Some default values if alignments are unspecified
        engine->alignAddr = 1;
        engine->alignLength = 1;
        engine->alignAddrBits = 64;
    }

    TraceVerbose(DBG_INIT, "%!FUNC! engine[%u][%u] alignments: bytes=%u, granularity=%u, addrBits=%u",
                 engine->channel, engine->dir, engine->alignAddr, engine->alignLength, engine->alignAddrBits);

}

UINT32 EngineStatus(IN XDMA_ENGINE *engine, IN BOOLEAN clear)
// read and optionally clear the engine status
{
    UINT32 status = 0;

    if (clear) { // request to clear engine status after read? 
        status = engine->regs->statusRC;
        TraceInfo(DBG_DPC, "%!FUNC! Cleared Engine Status after read");
    }
    else {
        status = engine->regs->status;
    }

    TraceVerbose(DBG_DMA, "%!FUNC! %s Engine %u status = 0x%08x:%s%s%s%s%s%s%s%s%s",
                 engine->dir == H2C ? "H2C" : "C2H", engine->channel, status,
                 (status & XDMA_BUSY_BIT) ? "BUSY " : "IDLE ",
                 (status & XDMA_DESCRIPTOR_STOPPED_BIT) ? "DESCRIPTOR_STOPPED " : " ",
                 (status & XDMA_DESCRIPTOR_COMPLETED_BIT) ? "DESCRIPTOR_COMPLETED " : " ",
                 (status & XDMA_ALIGN_MISMATCH_BIT) ? "ALIGN_MISMATCH " : " ",
                 (status & XDMA_MAGIC_STOPPED_BIT) ? "MAGIC_STOPPED " : " ",
                 (status & XDMA_FETCH_STOPPED_BIT) ? "FETCH_STOPPED" : " ",
                 (status & XDMA_STAT_READ_ERROR) ? "READ_ERROR " : " ",
                 (status & XDMA_STAT_DESCRIPTOR_ERROR) ? "DESCRIPTOR_ERROR " : " ",
                 (status & XDMA_IDLE_STOPPED_BIT) ? "IDLE_STOPPED" : " ");

	DbgPrint("%s Engine %u status = 0x%08x:%s%s%s%s%s%s%s%s%s",
		engine->dir == H2C ? "H2C" : "C2H", engine->channel, status,
		(status & XDMA_BUSY_BIT) ? "BUSY " : "IDLE ",
		(status & XDMA_DESCRIPTOR_STOPPED_BIT) ? "DESCRIPTOR_STOPPED " : " ",
		(status & XDMA_DESCRIPTOR_COMPLETED_BIT) ? "DESCRIPTOR_COMPLETED " : " ",
		(status & XDMA_ALIGN_MISMATCH_BIT) ? "ALIGN_MISMATCH " : " ",
		(status & XDMA_MAGIC_STOPPED_BIT) ? "MAGIC_STOPPED " : " ",
		(status & XDMA_FETCH_STOPPED_BIT) ? "FETCH_STOPPED" : " ",
		(status & XDMA_STAT_READ_ERROR) ? "READ_ERROR " : " ",
		(status & XDMA_STAT_DESCRIPTOR_ERROR) ? "DESCRIPTOR_ERROR " : " ",
		(status & XDMA_IDLE_STOPPED_BIT) ? "IDLE_STOPPED" : " ");
    return status;
}

VOID EngineStart(IN XDMA_ENGINE *engine)
// start the engine 
{
	UINT32 engineStatus;
    UINT32 regVal = XDMA_CTRL_RUN_BIT;
    if (engine->parentDevice->pollMode) {
        regVal |= XDMA_CTRL_POLL_MODE;
    }
    engine->regs->controlW1S = regVal;
    TraceInfo(DBG_DMA, "%!FUNC! engineRegs->control=0x%08x", engine->regs->control);
	DbgPrint("***************** Engine start:%x****************\n", engine->regs->control);
}

VOID EngineStop(IN XDMA_ENGINE *engine) {
    UINT32 regVal = XDMA_CTRL_RUN_BIT;
    if (engine->parentDevice->pollMode) {
        regVal |= XDMA_CTRL_POLL_MODE;
    }
    engine->regs->controlW1C = regVal;
    DbgPrint("Stopped Engine %u %u", engine->channel, engine->dir);
}

VOID EvtRequestCancel(IN WDFREQUEST request) {
    PQUEUE_CONTEXT queue = GetQueueContext(WdfRequestGetIoQueue(request));
    TraceInfo(DBG_IO, "%!FUNC! Request 0x%p from Queue 0x%p", request, queue);
    EngineStop(queue->engine);
    NTSTATUS status = WdfRequestUnmarkCancelable(request);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfRequestUnmarkCancelable failed: %!STATUS!", status);
    }
    status = WdfDmaTransactionRelease(queue->engine->dmaTransaction);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfDmaTransactionRelease failed: %!STATUS!", status);
    }
    WdfRequestComplete(request, STATUS_CANCELLED);
}

VOID EvtIoWriteDma(IN WDFQUEUE wdfQueue, IN WDFREQUEST Request, IN size_t length)
// callback for when a write I/O request enters the SGDMA write queue
{

    UNREFERENCED_PARAMETER(length);
    NTSTATUS status = STATUS_INTERNAL_ERROR;
    PQUEUE_CONTEXT  queue = GetQueueContext(wdfQueue);

    TraceVerbose(DBG_IO, "%!FUNC!(queue=%p, request=%p, length=%llu)", wdfQueue, Request, length);

    // initialize a DMA transaction from the request 
    status = WdfDmaTransactionInitializeUsingRequest(queue->engine->dmaTransaction, Request,
                                                     EvtProgramDma, WdfDmaDirectionWriteToDevice);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfDmaTransactionInitializeUsingRequest failed: %!STATUS!",
                   status);
        goto ErrExit;
    }
    status = WdfRequestMarkCancelableEx(Request, EvtRequestCancel);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfRequestMarkCancelableEx failed: %!STATUS!", status);
        goto ErrExit;
    }

    // supply the Queue as context for EvtProgramDma 
    status = WdfDmaTransactionExecute(queue->engine->dmaTransaction, queue->engine);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfDmaTransactionExecute failed: %!STATUS!", status);
        goto ErrExit;
    }

    if (queue->engine->parentDevice->pollMode) {
        status = EnginePollTransfer(queue->engine);
        if (!NT_SUCCESS(status)) {
            TraceError(DBG_IO, "%!FUNC! EnginePollTransfer failed: %!STATUS!", status);
            goto ErrExit;
        }
    }

    return; // success
ErrExit:
    WdfDmaTransactionRelease(queue->engine->dmaTransaction);
    WdfRequestComplete(Request, status);
    TraceError(DBG_IO, "%!FUNC! failed Request 0x%p: %!STATUS!", Request, status);
}

VOID EvtIoReadDma(IN WDFQUEUE wdfQueue, IN WDFREQUEST Request, IN size_t length)
// 
{
    UNREFERENCED_PARAMETER(length);
    NTSTATUS status = STATUS_INTERNAL_ERROR;
    PQUEUE_CONTEXT queue = GetQueueContext(wdfQueue);
	//DbgPrint(("(queue=%p, request=%p, length=%llu)\n", wdfQueue, Request, length));
    TraceVerbose(DBG_IO, "%!FUNC!(queue=%p, request=%p, length=%llu)", wdfQueue, Request, length);
	// DbgPrint("queue=%p, request=%p, length=%llu)", wdfQueue, Request, length);
    // initialize a DMA transaction from the request
    status = WdfDmaTransactionInitializeUsingRequest(queue->engine->dmaTransaction, Request, EvtProgramDma,
                                                     WdfDmaDirectionReadFromDevice);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfDmaTransactionInitializeUsingRequest failed: %!STATUS!",
                   status);
        goto ErrExit;
    }
    status = WdfRequestMarkCancelableEx(Request, EvtRequestCancel);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfRequestMarkCancelableEx failed: %!STATUS!", status);
        goto ErrExit;
    }

    // supply the Queue as context for EvtProgramDma
    status = WdfDmaTransactionExecute(queue->engine->dmaTransaction, queue->engine);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfDmaTransactionExecute failed: %!STATUS!", status);
        goto ErrExit;
    }

    if (queue->engine->parentDevice->pollMode) {
        status = EnginePollTransfer(queue->engine);
        if (!NT_SUCCESS(status)) {
            TraceError(DBG_IO, "%!FUNC! EnginePollTransfer failed: %!STATUS!", status);
            goto ErrExit;
        }
    }

    return; // success
ErrExit:
    WdfDmaTransactionRelease(queue->engine->dmaTransaction);
    WdfRequestComplete(Request, status);
    TraceError(DBG_IO, "%!FUNC! failed Request 0x%p: %!STATUS!", Request, status);
}

VOID EngineEnableInterrupt(IN XDMA_ENGINE* engine) {
    if (!engine) {
        TraceError(DBG_IRQ, "%!FUNC! engine=%p", engine);
        return;
    
    }
    XDMA_IRQ_REGS* regs = engine->parentDevice->interruptRegs;
    regs->channelIntEnableW1S = engine->irqBitMask;
    TraceInfo(DBG_IRQ, "%!FUNC! mask now 0x%08x", regs->channelIntEnable);
}

VOID EngineDisableInterrupt(IN XDMA_ENGINE* engine) {
    if (!engine) {
        TraceError(DBG_IRQ, "%!FUNC! engine=%p", engine);
        return;
    }
    XDMA_IRQ_REGS* regs = engine->parentDevice->interruptRegs;
    regs->channelIntEnableW1C = engine->irqBitMask;
    TraceInfo(DBG_IRQ, "%!FUNC! mask now 0x%08x", regs->channelIntEnable);
}

// ========================= streaming engine ============================================

static NTSTATUS EngineCreateResultBuffer(IN XDMA_ENGINE* engine, IN WDFOBJECT device) {

	UNREFERENCED_PARAMETER(device);
	// create dma result buffer
	size_t resultBufferSize = (MAX_TRANSFER_SIZE / PAGE_SIZE + 2) * sizeof(DMA_RESULT);
	NTSTATUS status = WdfCommonBufferCreate(engine->parentDevice->dmaEnabler, resultBufferSize,
		WDF_NO_OBJECT_ATTRIBUTES, &engine->ring.results);
	if (!NT_SUCCESS(status)) {
		TraceError(DBG_INIT, "%!FUNC! WdfCommonBufferCreate failed: %!STATUS!", status);
		return status;
	}
	PUCHAR resultBufferVA = (PUCHAR)WdfCommonBufferGetAlignedVirtualAddress(engine->ring.results);
	RtlZeroMemory(resultBufferVA, resultBufferSize);

	TraceVerbose(DBG_INIT, "%!FUNC! engine[%u][%u] dma result buffer @ pa=0x%08llx",
		engine->channel, engine->dir,
		WdfCommonBufferGetAlignedLogicalAddress(engine->ring.results).QuadPart);

	return status;
}

static NTSTATUS EngineCreateRingBuffer(IN XDMA_ENGINE* engine, IN WDFOBJECT device) {

    UNREFERENCED_PARAMETER(device);
    // create dma result buffer
    size_t resultBufferSize = (MAX_TRANSFER_SIZE / PAGE_SIZE + 2) * sizeof(DMA_RESULT);
    NTSTATUS status = WdfCommonBufferCreate(engine->parentDevice->dmaEnabler, resultBufferSize,
                                            WDF_NO_OBJECT_ATTRIBUTES, &engine->ring.results);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfCommonBufferCreate failed: %!STATUS!", status);
        return status;
    }
    PUCHAR resultBufferVA = (PUCHAR)WdfCommonBufferGetAlignedVirtualAddress(engine->ring.results);
    RtlZeroMemory(resultBufferVA, resultBufferSize);

    TraceVerbose(DBG_INIT, "%!FUNC! engine[%u][%u] dma result buffer @ pa=0x%08llx",
                 engine->channel, engine->dir,
                 WdfCommonBufferGetAlignedLogicalAddress(engine->ring.results).QuadPart);

    // create dma data buffer
    PHYSICAL_ADDRESS low, high, skip;
    low.QuadPart = 0;
    high.QuadPart = 0xFFFFFFFFFFFFFFFF;
    skip.QuadPart = PAGE_SIZE;
    PMDL mdl = MmAllocatePagesForMdlEx(low, high, skip, XDMA_RING_NUM_BLOCKS * XDMA_RING_BLOCK_SIZE, MmNonCached, NormalPagePriority);
    if (!mdl) {
        TraceError(DBG_INIT, "%!FUNC! MmAllocatePagesForMdlEx failed!");
        return STATUS_INTERNAL_ERROR;
    }

    PVOID rxBufferVa = MmMapLockedPagesSpecifyCache(mdl, KernelMode, MmNonCached, NULL, FALSE, NormalPagePriority);
    if (!rxBufferVa) {
        TraceError(DBG_INIT, "%!FUNC! MmMapLockedPagesSpecifyCache failed!");
        return STATUS_INTERNAL_ERROR;
    }

    TraceInfo(DBG_INIT, "%!FUNC! mdl VA=%p, byteCount=%u, next=%p", rxBufferVa, MmGetMdlByteCount(mdl), mdl->Next);

    // MDL ring
    for (UINT i = 0; i < XDMA_RING_NUM_BLOCKS; ++i) {
        engine->ring.mdl[i] = IoAllocateMdl((PUCHAR)rxBufferVa + (i * XDMA_RING_BLOCK_SIZE), XDMA_RING_BLOCK_SIZE, TRUE, FALSE, NULL);
        if (!engine->ring.mdl[i]) {
            TraceError(DBG_INIT, "%!FUNC! IoAllocateMdl failed!");
            return STATUS_INTERNAL_ERROR;
        }
    }

    for (UINT i = 0; i < XDMA_RING_NUM_BLOCKS; ++i) {
        TraceInfo(DBG_INIT, "%!FUNC! sub-mdl VA=%p, byteCount=%u, next=%p",
                  MmGetMdlVirtualAddress(engine->ring.mdl[i]),
                  MmGetMdlByteCount(engine->ring.mdl[i]),
                  engine->ring.mdl[i]->Next);
    }

    status = WdfSpinLockCreate(WDF_NO_OBJECT_ATTRIBUTES, &engine->ring.lock);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfSpinLockCreate failed: %!STATUS!", status);
        return status;
    }

    return status;
}

static VOID EngineRingAdvance(UINT* index) {
    if (*index == XDMA_RING_NUM_BLOCKS - 1) { // wrap-around
        *index = 0;
    } else { // normal increment
        ++(*index);
    }
}

static UINT EngineProcessRing(IN XDMA_ENGINE *engine) {

    UINT32 engineStatus = EngineStatus(engine, TRUE);
    TraceInfo(DBG_DPC, "%!FUNC! %s Engine Status: 0x%08x",
              engine->dir == H2C ? "H2C" : "C2H", engineStatus);
    UINT eopCount = 0;
    DMA_RESULT* results = (DMA_RESULT*)WdfCommonBufferGetAlignedVirtualAddress(engine->ring.results);

    WdfSpinLockAcquire(engine->ring.lock);
    UINT tail = engine->ring.tail;
    UINT head = engine->ring.head;


    TraceInfo(DBG_DPC, "%!FUNC! head=%u, tail=%u, overflow=%u", head, tail, engine->ring.overflow);

    while (results[tail].status && !engine->ring.overflow) {
        TraceInfo(DBG_DPC, "%!FUNC! dma results: status=0x%08x, length=%u",
                  results[tail].status, results[tail].length);

        if (results[tail].status & XDMA_RESULT_EOP_BIT) {
            eopCount++;
        }

        // mark current dma result as processed
        results[tail].status = 0;

        EngineRingAdvance(&tail);

        if (tail == head) {
            engine->ring.overflow = TRUE;
            break;
        }
    }

    TraceInfo(DBG_DPC, "%!FUNC! head=%u, tail=%u, eop=%u, overflow=%u",
              head, tail, eopCount, engine->ring.overflow);
    engine->ring.tail = tail;
    WdfSpinLockRelease(engine->ring.lock);


    // If any packets are completed, start the Io Read queue 
    // also start the queue on an overflow since we need to tell the client that an overflow happened
    if (eopCount > 0 || (engine->ring.overflow == TRUE)) {
        TraceVerbose(DBG_DPC, "%!FUNC! starting engine queue");
        WdfIoQueueStart(engine->queue);
    }

    // clear poll writeback buffer
    if (engine->parentDevice->pollMode) {
        XDMA_POLL_WB* wbBuffer = (XDMA_POLL_WB*)WdfCommonBufferGetAlignedVirtualAddress(engine->pollWbBuffer);
        size_t wbBufferLength = WdfCommonBufferGetLength(engine->pollWbBuffer);
        RtlZeroMemory(wbBuffer, wbBufferLength);
        engine->numDescriptors = 0;
    }

    return eopCount;
}

static VOID EngineRingProgramDma(IN XDMA_ENGINE* engine) {

    // get virtual and physical pointers to descriptor buffer
    DMA_DESCRIPTOR *descriptor = (DMA_DESCRIPTOR*)WdfCommonBufferGetAlignedVirtualAddress(engine->descBuffer);
    PHYSICAL_ADDRESS nextDescLA = WdfCommonBufferGetAlignedLogicalAddress(engine->descBuffer);

    // get physical address to dma result buffer
    PHYSICAL_ADDRESS resultBufferLA = WdfCommonBufferGetAlignedLogicalAddress(engine->ring.results);

    //for (ULONG i = 0; i < SgList->NumberOfElements; ++i) {
    for (ULONG i = 0; i < XDMA_RING_NUM_BLOCKS; ++i) {
        descriptor[i].control = (XDMA_DESC_MAGIC | XDMA_DESC_EOP_BIT | XDMA_DESC_COMPLETED_BIT);
        descriptor[i].numBytes = XDMA_RING_BLOCK_SIZE;

        // source address are unused, will be overwritten by hardware with dma result
        descriptor[i].srcAddrLo = resultBufferLA.LowPart;
        descriptor[i].srcAddrHi = resultBufferLA.HighPart;
        resultBufferLA.QuadPart += sizeof(DMA_RESULT);

        // destination is host memory
        PHYSICAL_ADDRESS dst = MmGetPhysicalAddress(MmGetMdlVirtualAddress(engine->ring.mdl[i]));
        descriptor[i].dstAddrLo = dst.LowPart;
        descriptor[i].dstAddrHi = dst.HighPart;

        // next descriptor bus address 
        nextDescLA.QuadPart += sizeof(DMA_DESCRIPTOR);
        descriptor[i].nextLo = nextDescLA.LowPart;
        descriptor[i].nextHi = nextDescLA.HighPart;

        if (FALSE == DescriptorIsAligned(engine, descriptor)) {
            TraceWarning(DBG_DMA, "%!FUNC! failed: Dma Transfer is not aligned");
        }

        DumpDescriptor(&(descriptor[i]));
    }

    // make the discriptor list circular
    nextDescLA = WdfCommonBufferGetAlignedLogicalAddress(engine->descBuffer);
    DMA_DESCRIPTOR* last = &descriptor[XDMA_RING_NUM_BLOCKS - 1];
    last->nextLo = nextDescLA.LowPart;
    last->nextHi = nextDescLA.HighPart;

    DumpDescriptor(last);

    TraceVerbose(DBG_DMA, "%!FUNC! first desc @ 0x%08x%08x",
                 engine->sgdma->firstDescHi, engine->sgdma->firstDescLo);
    TraceVerbose(DBG_DMA, "%!FUNC! last desc points to 0x%08x%08x", last->nextHi, last->nextLo);

    if (engine->parentDevice->pollMode) {
        engine->numDescriptors = 0;
    }

    MemoryBarrier();
	DbgPrint("EngineRingProgramDma EngineStart\n");
    // start the engine
    EngineStart(engine);

    MemoryBarrier();
}

static VOID EngineClearDmaResults(IN XDMA_ENGINE *engine) {
    TraceVerbose(DBG_DMA, "%!FUNC! clearing DMA results...");
    DMA_RESULT * results = (DMA_RESULT*)WdfCommonBufferGetAlignedVirtualAddress(engine->ring.results);
    for (UINT i = 0; i < XDMA_RING_NUM_BLOCKS; ++i) {
        results[i].status = 0;
        results[i].length = 0;
    }
}

VOID EngineRingSetup(IN XDMA_ENGINE *engine) {
    engine->ring.head = 0;
    engine->ring.tail = 0;
    EngineRingProgramDma(engine);
}

VOID EngineRingTeardown(IN XDMA_ENGINE *engine) {
    EngineStop(engine);
    engine->ring.head = 0;
    engine->ring.tail = 0;
}

VOID EvtIoReadEngineRing(IN WDFQUEUE wdfQueue, IN WDFREQUEST Request, IN size_t length)
// 
{
    NTSTATUS status = STATUS_UNSUCCESSFUL;
    PQUEUE_CONTEXT queue = GetQueueContext(wdfQueue);
    XDMA_ENGINE* engine = queue->engine;
    size_t numBytes = 0;

    // get output buffer
    WDFMEMORY outputMem;
    status = WdfRequestRetrieveOutputMemory(Request, &outputMem);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_IO, "%!FUNC! WdfRequestRetrieveOutputMemory failed: %!STATUS!", status);
        goto ErrorExit;
    }

    DMA_RESULT* results = (DMA_RESULT*)WdfCommonBufferGetAlignedVirtualAddress(engine->ring.results);
    size_t numBytesRemaining = length;
    size_t offset = 0;

    TraceInfo(DBG_IO, "%!FUNC! request length = %llu", length);

    WdfSpinLockAcquire(engine->ring.lock);
    UINT head = engine->ring.head;
    UINT tail = engine->ring.tail;

    TraceInfo(DBG_IO, "%!FUNC! head=%u, tail=%u overflow=%u", head, tail, engine->ring.overflow);

    if (engine->ring.overflow == TRUE) {
        status = STATUS_ALLOTTED_SPACE_EXCEEDED;
        EngineClearDmaResults(engine);
        EngineRingTeardown(engine);
        EngineRingSetup(engine);
        engine->ring.overflow = FALSE;
        goto ErrorReleaseLockExit;
    }

    while ((head != tail) && numBytesRemaining) {

        // get dma ring buffer address and bytes transferred
        PVOID rxBufferVa = MmGetMdlVirtualAddress(engine->ring.mdl[head]);
        size_t numBytesReceived = results[head].length;

        TraceInfo(DBG_IO, "%!FUNC! head=%u, bufferVA=%p, bufferSize=%llu",
                  head, rxBufferVa, numBytesReceived);
        TraceInfo(DBG_IO, "%!FUNC! numBytesRemaining=%llu", numBytesRemaining);

        // limit buffer size
        if (numBytesReceived > numBytesRemaining) {
            numBytesReceived = numBytesRemaining;
        } else if (numBytesReceived == 0) {
            break;
        }
        // expect result status to have been cleared by EngineProcessRing
        // otherwise it must have been overwritten by fpga before we had a chance to send this  
        // chunk to user-space. Therefore overflow
        if (results[head].status != 0) {
            status = STATUS_ALLOTTED_SPACE_EXCEEDED; //  STATUS_UNSUCCESSFUL
            EngineClearDmaResults(engine);
            EngineRingTeardown(engine);
            EngineRingSetup(engine);
            engine->ring.overflow = FALSE;
            goto ErrorReleaseLockExit;
        }

        // copy to user
        status = WdfMemoryCopyFromBuffer(outputMem, offset, rxBufferVa, numBytesReceived);
        if (!NT_SUCCESS(status)) {
            TraceError(DBG_IO, "%!FUNC! WdfMemoryCopyFromBuffer failed: %!STATUS!", status);
            goto ErrorReleaseLockExit;
        }

        offset += numBytesReceived;
        numBytesRemaining -= numBytesReceived;

        results[head].length = 0;
        EngineRingAdvance(&head);
    }

    TraceInfo(DBG_IO, "%!FUNC! head=%u, tail=%u overflow=%u", head, tail, engine->ring.overflow);


    engine->ring.head = head;
    numBytes = length - numBytesRemaining;

    WdfSpinLockRelease(engine->ring.lock);
    WdfRequestCompleteWithInformation(Request, status, numBytes);
    return;

ErrorReleaseLockExit:
    WdfSpinLockRelease(engine->ring.lock);
ErrorExit:
    TraceInfo(DBG_IO, "%!FUNC! returning %!STATUS!", status);
    WdfRequestComplete(Request, status);

}

//========================= polling interface =====================================================

static NTSTATUS EngineCreatePollWriteBackBuffer(IN OUT XDMA_ENGINE *engine) {
    // allocate host-side buffer for poll mode descriptor write back info

    NTSTATUS status = WdfCommonBufferCreate(engine->parentDevice->dmaEnabler, sizeof(XDMA_POLL_WB),
                                            WDF_NO_OBJECT_ATTRIBUTES, &engine->pollWbBuffer);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfCommonBufferCreate failed: %!STATUS!", status);
        return status;
    }

    PHYSICAL_ADDRESS wbBufferLA = WdfCommonBufferGetAlignedLogicalAddress(engine->pollWbBuffer);
    PUCHAR wbBufferVA = (PUCHAR)WdfCommonBufferGetAlignedVirtualAddress(engine->pollWbBuffer);
    RtlZeroMemory(wbBufferVA, sizeof(XDMA_POLL_WB));

    engine->regs->pollModeWbLo = wbBufferLA.LowPart;
    engine->regs->pollModeWbHi = wbBufferLA.HighPart;

    TraceInfo(DBG_INIT, "%!FUNC! poll wb buffer at 0x%08x%08x, size=%lld",
              engine->regs->pollModeWbHi, engine->regs->pollModeWbLo, sizeof(XDMA_POLL_WB));
    return status;
}

NTSTATUS EnginePollTransfer(IN XDMA_ENGINE* engine) {

    XDMA_POLL_WB* writeback_data = (XDMA_POLL_WB*)WdfCommonBufferGetAlignedVirtualAddress(engine->pollWbBuffer);
    const ULONG expected = engine->numDescriptors;
    volatile ULONG actual = 0;

    do {
        actual = writeback_data->completedDescCount;

        if (actual & XDMA_WB_ERR_MASK) {
            TraceError(DBG_DMA, "%!FUNC! error on writeback %u", actual);
            return STATUS_INTERNAL_ERROR;
        }
        actual &= XDMA_WB_COUNT_MASK;

        if (actual > expected) {
            actual = expected | XDMA_WB_ERR_MASK;
        }
    } while (expected != actual);

    EngineProcessTransfer(engine);

    return STATUS_SUCCESS;
}

NTSTATUS EnginePollRing(IN XDMA_ENGINE* engine) {
    XDMA_POLL_WB* writeback_data = (XDMA_POLL_WB*)WdfCommonBufferGetAlignedVirtualAddress(engine->pollWbBuffer);
    volatile ULONG completed = 0;
    volatile UINT eopCount = 0;
    UINT tryCount = 0;
    do {
        completed = writeback_data->completedDescCount;


        if (completed & XDMA_WB_ERR_MASK) {
            TraceError(DBG_DMA, "%!FUNC! error on writeback %u", completed);
            return STATUS_INTERNAL_ERROR;
        }
        if (completed) {
            eopCount = EngineProcessRing(engine);
            TraceVerbose(DBG_DMA, "%!FUNC! complete=%u, eop=%u", completed, eopCount);
        }
        tryCount++;
        if (tryCount >= 100000) {
            break;
        }

    } while (!eopCount && !engine->ring.overflow);

    TraceVerbose(DBG_DMA, "%!FUNC! complete=%u, eop=%u, tryCount=%u, overflow=%u",
                 completed, eopCount, tryCount, engine->ring.overflow);

    return STATUS_SUCCESS;
}

//========================= performance counters interface ========================================

VOID EngineStartPerf(IN XDMA_ENGINE* engine) {
    ASSERTMSG("argument engine is NULL!", engine != NULL);

    // Automatically stops performance counters when a descriptor with the stop bit is completed.
    engine->regs->perfCtrl = XDMA_PERF_CLEAR;
    engine->regs->perfCtrl = XDMA_PERF_AUTO | XDMA_PERF_RUN;
}

VOID EngineGetPerf(IN XDMA_ENGINE* engine, OUT XDMA_PERF_DATA* perfData) {
    TraceVerbose(DBG_DMA, "%!FUNC! cycleCount=0x%08x%08x dataCount=0x%08x%08x pendingCount=0x%08x%08x", 
                 engine->regs->perfCycHi, engine->regs->perfCycLo, 
                 engine->regs->perfDatHi, engine->regs->perfDatLo, 
                 engine->regs->perfPndHi, engine->regs->perfPndLo);

    ASSERTMSG("argument engine is NULL!", engine != NULL);
    ASSERTMSG("argument perfData is NULL!", perfData != NULL);

    perfData->clockCycleCount = ((UINT64)engine->regs->perfCycHi << 32) + engine->regs->perfCycLo;
    perfData->dataCycleCount = ((UINT64)engine->regs->perfDatHi << 32) + engine->regs->perfDatLo;
    perfData->pendingCount = ((UINT64)engine->regs->perfPndHi << 32) + engine->regs->perfPndHi;
}