/*
* XDMA Device Driver for Windows
* ===============================
* 
* Copyright 2017 Xilinx Inc.
* Copyright 2010-2012 Sidebranch
* Copyright 2010-2012 Leon Woestenberg <leon@sidebranch.com>
*
* Maintainer:
* -----------
* Alexander Hornburg <alexander.hornburg@xilinx.com>
*
* Description:
* ------------
* This is a sample driver for the Xilinx Inc. 'DMA/Bridge Subsystem for PCIe v3.0' (XDMA) IP.
*
* References:
* -----------
*	[1] pg195-pcie-dma.pdf - DMA/Bridge Subsystem for PCI Express v3.0 - Product Guide
*/

// ========================= include dependencies =================================================

#include "device.h"
#include "trace.h"

#ifdef DBG
// The trace message header (.tmh) file must be included in a source file before any WPP macro 
// calls and after defining a WPP_CONTROL_GUIDS macro (defined in trace.h). see trace.h
#include "driver.tmh"
#endif

// ========================= declarations ================================================= 

DRIVER_INITIALIZE DriverEntry;
DRIVER_UNLOAD DriverUnload;

// Mark these functions as pageable code
#ifdef ALLOC_PRAGMA
#pragma alloc_text (INIT, DriverEntry)
#pragma alloc_text (PAGE, DriverUnload)
#endif

// ========================= definitions =================================================

const char * const dateTimeStr = "Built " __DATE__ ", " __TIME__ ".";

// main entry point - Called when driver is installed
NTSTATUS DriverEntry(IN PDRIVER_OBJECT driverObject, IN PUNICODE_STRING registryPath) {
    NTSTATUS			status = STATUS_SUCCESS;
    WDF_DRIVER_CONFIG	DriverConfig;
    WDFDRIVER			Driver;

    // Initialize WPP Tracing
    WPP_INIT_TRACING(driverObject, registryPath);
    TraceInfo(DBG_INIT, "XDMA Driver - %s", dateTimeStr);

    // Initialize the Driver Config; register the device add event callback
    // EvtDeviceAdd() will be called when a device is found
    WDF_DRIVER_CONFIG_INIT(&DriverConfig, EvtDeviceAdd);

    // Creates a WDFDRIVER object, the top of our device's tree of objects
    status = WdfDriverCreate(driverObject, registryPath, WDF_NO_OBJECT_ATTRIBUTES, &DriverConfig,
                             &Driver);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfDriverCreate failed: %!STATUS!", status);
        WPP_CLEANUP(driverObject);
        return status;
    }

    driverObject->DriverUnload = DriverUnload;

    return status;
}

// Called before the driver is removed
VOID DriverUnload(IN PDRIVER_OBJECT driverObject) {
    PAGED_CODE();
    UNREFERENCED_PARAMETER(driverObject);
    TraceVerbose(DBG_INIT, "%!FUNC!");

    WPP_CLEANUP(driverObject); // cleanup tracing

    return;
}
