/*
* XDMA Interrupt Service Routines, Handlers and Dispatch Functions
* ===============================
* 
* Copyright 2017 Xilinx Inc.
* Copyright 2010-2012 Sidebranch
* Copyright 2010-2012 Leon Woestenberg <leon@sidebranch.com>
*
* Maintainer:
* -----------
* Alexander Hornburg <alexande@xilinx.com>
*
*/
// ========================= include dependencies =================================================
#include "device.h"
#include "dma_engine.h"
#include "interrupt.h"

#include "trace.h"
#ifdef DBG
// The trace message header (.tmh) file must be included in a source file before any WPP macro 
// calls and after defining a WPP_CONTROL_GUIDS macro (defined in trace.h). see trace.h
#include "interrupt.tmh"
#endif

// ====================== static helper functions =================================================

static UINT32 BuildVectorReg(UINT32 a, UINT32 b, UINT32 c, UINT32 d) {
    UINT32 reg_val = 0;
    reg_val |= (a & 0x1f) << 0;
    reg_val |= (b & 0x1f) << 8;
    reg_val |= (c & 0x1f) << 16;
    reg_val |= (d & 0x1f) << 24;
    return reg_val;
}

static NTSTATUS SetupUserInterrupt(IN WDFDEVICE device, IN ULONG index, 
                                   IN PCM_PARTIAL_RESOURCE_DESCRIPTOR resource,
                                   IN PCM_PARTIAL_RESOURCE_DESCRIPTOR translatedResource) {
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    WDF_INTERRUPT_CONFIG config;
    WDF_INTERRUPT_CONFIG_INIT(&config, EvtUserInterruptIsr, EvtUserInterruptDpc);
    config.InterruptRaw = resource;
    config.InterruptTranslated = translatedResource;
    config.EvtInterruptEnable = EvtUserInterruptEnable;
    config.EvtInterruptDisable = EvtUserInterruptDisable;

    WDF_OBJECT_ATTRIBUTES attribs;
    WDF_OBJECT_ATTRIBUTES_INIT(&attribs);
    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&attribs, IRQ_CONTEXT);

    NTSTATUS status = WdfInterruptCreate(device, &config, &attribs, &(xdma->userEvents[index].irq));
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfInterruptCreate failed: %!STATUS!", status);
    }

    PIRQ_CONTEXT irqContext = GetIrqContext(xdma->userEvents[index].irq);
    irqContext->eventId = index; // msg Id = irq index = event id
    return status;
}

static NTSTATUS SetupChannelInterrupt(IN WDFDEVICE device, IN ULONG index,
                                      IN PCM_PARTIAL_RESOURCE_DESCRIPTOR resource,
                                      IN PCM_PARTIAL_RESOURCE_DESCRIPTOR translatedResource) {
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    WDF_INTERRUPT_CONFIG config;
    WDF_INTERRUPT_CONFIG_INIT(&config, EvtChannelInterruptIsr, EvtChannelInterruptDpc);
    config.InterruptRaw = resource;
    config.InterruptTranslated = translatedResource;
    config.EvtInterruptEnable = EvtChannelInterruptEnable;
    config.EvtInterruptDisable = EvtChannelInterruptDisable;

    WDF_OBJECT_ATTRIBUTES attribs;
    WDF_OBJECT_ATTRIBUTES_INIT(&attribs);
    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&attribs, IRQ_CONTEXT);

    NTSTATUS status = WdfInterruptCreate(device, &config, &attribs, &(xdma->channelInterrupts[index]));
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfInterruptCreate failed: %!STATUS!", status);
    }
    return status;
}

static NTSTATUS SetupDeviceInterrupt(IN WDFDEVICE device, IN PCM_PARTIAL_RESOURCE_DESCRIPTOR resource,
                                     IN PCM_PARTIAL_RESOURCE_DESCRIPTOR translatedResource) {
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    WDF_INTERRUPT_CONFIG config;
    WDF_INTERRUPT_CONFIG_INIT(&config, EvtInterruptIsr, EvtInterruptDpc);
    config.InterruptRaw = resource;
    config.InterruptTranslated = translatedResource;
    config.EvtInterruptEnable = EvtInterruptEnable;
    config.EvtInterruptDisable = EvtInterruptDisable;

    WDF_OBJECT_ATTRIBUTES attribs;
    WDF_OBJECT_ATTRIBUTES_INIT(&attribs);
    WDF_OBJECT_ATTRIBUTES_INIT_CONTEXT_TYPE(&attribs, IRQ_CONTEXT);

    NTSTATUS status = WdfInterruptCreate(device, &config, &attribs, &xdma->lineInterrupt);
    if (!NT_SUCCESS(status)) {
        TraceError(DBG_INIT, "%!FUNC! WdfInterruptCreate failed: %!STATUS!", status);
    }

    return status;
}

// ====================== setup functions =========================================================

NTSTATUS SetupSingleInterruptResource(IN WDFDEVICE device, IN WDFCMRESLIST ResourcesRaw,
                                      IN WDFCMRESLIST ResourcesTranslated) {

    PXDMA_DEVICE xdma = GetDeviceContext(device);
    PCM_PARTIAL_RESOURCE_DESCRIPTOR resource;
    PCM_PARTIAL_RESOURCE_DESCRIPTOR resourceRaw;
    NTSTATUS status = STATUS_INTERNAL_ERROR;
    ULONG numResources = WdfCmResourceListGetCount(ResourcesTranslated);
    UINT32 vectorValue = 0;

    ASSERT(xdma->interruptRegs != NULL);

    for (UINT i = 0; i < numResources; i++) {
        resource = WdfCmResourceListGetDescriptor(ResourcesTranslated, i);
        resourceRaw = WdfCmResourceListGetDescriptor(ResourcesRaw, i);

        if (resource->Type != CmResourceTypeInterrupt) {
            continue;
        }

        status = SetupDeviceInterrupt(device, resourceRaw, resource);
        if (!NT_SUCCESS(status)) {
            TraceError(DBG_INIT, "%!FUNC! failed to setup device interrupt: %!STATUS!", status);
        }

        if (!(resource->Flags & CM_RESOURCE_INTERRUPT_MESSAGE)) { // LINE interrupt
            // Windows: A=1, B=2, C=3, D=4 ?
            // XDMA:	A=0, B=1, C=2, D=3
            vectorValue = xdma->pciHeader.u.type0.InterruptPin - 1;
        }

        TraceVerbose(DBG_INIT, "%!FUNC! found interrupt resource.");

        status = STATUS_SUCCESS;
        break;
    }

    TraceVerbose(DBG_INIT, "%!FUNC! InterruptLine value = %u", vectorValue);

    xdma->interruptRegs->userVector[0] = vectorValue;
    xdma->interruptRegs->userVector[1] = vectorValue;
    xdma->interruptRegs->userVector[2] = vectorValue;
    xdma->interruptRegs->userVector[3] = vectorValue;
    xdma->interruptRegs->channelVector[0] = vectorValue;
    xdma->interruptRegs->channelVector[1] = vectorValue;

    return status;
}

NTSTATUS SetupMultipleInterruptResources(IN WDFDEVICE device, IN WDFCMRESLIST ResourcesRaw,
                                         IN WDFCMRESLIST ResourcesTranslated) {

    PXDMA_DEVICE xdma = GetDeviceContext(device);
    PCM_PARTIAL_RESOURCE_DESCRIPTOR resource;
    PCM_PARTIAL_RESOURCE_DESCRIPTOR resourceRaw;
    NTSTATUS status = STATUS_SUCCESS;
    ULONG numResources = WdfCmResourceListGetCount(ResourcesTranslated);
    ULONG interruptCount = 0;

    ASSERT(xdma->interruptRegs != NULL);

    for (UINT i = 0; (i < numResources) && (interruptCount < XMDA_MAX_NUM_IRQ); i++) {

        resource = WdfCmResourceListGetDescriptor(ResourcesTranslated, i);
        resourceRaw = WdfCmResourceListGetDescriptor(ResourcesRaw, i);

        if (resource->Type != CmResourceTypeInterrupt) {
            continue;
        }

        TraceVerbose(DBG_INIT, "%!FUNC! msgCount=%u, Vector=%u",
                     resource->u.MessageInterrupt.Raw.MessageCount,
                     resource->u.MessageInterrupt.Translated.Vector);

        // assign first 16 interrupt resources to user events
        if (interruptCount < XDMA_MAX_USER_IRQ) {
            status = SetupUserInterrupt(device, interruptCount, resourceRaw, resource);
        } 
        else { // assign next 8 interrupt resources to dma engines
            status = SetupChannelInterrupt(device, interruptCount - XDMA_MAX_USER_IRQ, resourceRaw, 
                                           resource);
        }
        if (!NT_SUCCESS(status)) {
            TraceError(DBG_INIT, "%!FUNC! failed to setup device interrupt: %!STATUS!", status);
            return status;
        }

        ++interruptCount;

    }

    if (interruptCount != XMDA_MAX_NUM_IRQ) {
        TraceError(DBG_INIT, "%!FUNC! too few interrupt resources! %u", interruptCount);
    }

    // first 16 msg IDs are user irq
    xdma->interruptRegs->userVector[0] = BuildVectorReg(0, 1, 2, 3);
    xdma->interruptRegs->userVector[1] = BuildVectorReg(4, 5, 6, 7);
    xdma->interruptRegs->userVector[2] = BuildVectorReg(8, 9, 10, 11);
    xdma->interruptRegs->userVector[3] = BuildVectorReg(12, 13, 14, 15);

    // next 8 are dma channel
    xdma->interruptRegs->channelVector[0] = BuildVectorReg(16, 17, 18, 19);
    xdma->interruptRegs->channelVector[1] = BuildVectorReg(20, 21, 22, 23);

    return status;
}
// ====================== line/msi interrupt callback functions ===================================

NTSTATUS EvtInterruptEnable(IN WDFINTERRUPT Interrupt, IN WDFDEVICE device) {
    UNREFERENCED_PARAMETER(Interrupt);
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    xdma->interruptRegs->channelIntEnableW1S = 0xFFFFFFFFUL;
    xdma->interruptRegs->userIntEnableW1S = 0xFFFFFFFFUL;
    TraceVerbose(DBG_IRQ, "%!FUNC! enabled ALL interrupts");
    return STATUS_SUCCESS;
}

NTSTATUS EvtInterruptDisable(IN WDFINTERRUPT Interrupt, IN WDFDEVICE device) {
    UNREFERENCED_PARAMETER(Interrupt);
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    xdma->interruptRegs->channelIntEnableW1C = 0xFFFFFFFFUL;
    xdma->interruptRegs->userIntEnableW1C = 0xFFFFFFFFUL;
    TraceVerbose(DBG_IRQ, "%!FUNC! disabled ALL interrupts");
    return STATUS_SUCCESS;
}

BOOLEAN EvtInterruptIsr(IN WDFINTERRUPT Interrupt, IN ULONG MessageID)
// interrupt service routine - handle line interrupts
{
    PXDMA_DEVICE xdma = GetDeviceContext(WdfInterruptGetDevice(Interrupt));
    PIRQ_CONTEXT irq = GetIrqContext(Interrupt);
    XDMA_IRQ_REGS* irqRegs = NULL;
    UINT32 chIrq = 0;
    UINT32 userIrq = 0;
	DbgPrint("*************** EvtInterruptIsr ****************\n");
    TraceInfo(DBG_IRQ, "%!FUNC! message id = %u", MessageID);
	//DbgPrint("message id = %u\n", MessageID);

    ASSERTMSG("isr irq context NULL", irq != NULL);
    ASSERTMSG("isr device context NULL", xdma != NULL);
    irqRegs = xdma->interruptRegs;
    ASSERTMSG("isr device regs NULL", irqRegs != NULL);

    // read channel interrupt request registers
    // channel interrupt(s) requested?
    chIrq = xdma->interruptRegs->channelIntRequest;
    TraceInfo(DBG_IRQ, "%!FUNC! chan EN=0x%08X RQ=0x%08X PE=0x%08X\n",
              irqRegs->channelIntEnable, irqRegs->channelIntRequest, irqRegs->channelIntPending);

	/*DbgPrint("chan EN=0x%08X RQ=0x%08X PE=0x%08X\n",
		irqRegs->channelIntEnable, irqRegs->channelIntRequest, irqRegs->channelIntPending);*/
    if (chIrq) {
        irq->channelIrqPending |= chIrq; // remember fired channel interrupts
        irqRegs->channelIntEnableW1C = chIrq; // disable fired channel interrupts
    }

    // read user interrupts that are pending in the controller - flushes previous write
    // user interrupt(s) requested?
    userIrq = irqRegs->userIntRequest;
    TraceInfo(DBG_IRQ, "%!FUNC! user EN=0x%08X RQ=0x%08X PE=0x%08X",
              irqRegs->userIntEnable, irqRegs->userIntRequest, irqRegs->userIntPending);
    *(UINT32*)((char*)xdma->bar[xdma->userBarIdx]) = userIrq;
    if (userIrq) {
        irq->userIrqPending |= userIrq; // remember fired user interrupts
        irqRegs->userIntEnableW1C = userIrq; // disable fired user interrupts
    }

    // was the interrupt handled correctly?
    if (!chIrq && !userIrq) {
        TraceWarning(DBG_IRQ, "%!FUNC! Spurious interrupt");
		DbgPrint("Spurious interrupt");
        return FALSE;
    }

    // schedule deferred work
    WdfInterruptQueueDpcForIsr(Interrupt);
    return TRUE;
}

VOID EvtInterruptDpc(IN WDFINTERRUPT interrupt, IN WDFOBJECT device)
// Deferred interrupt service handler
{
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    PIRQ_CONTEXT irq = GetIrqContext(interrupt);
    XDMA_IRQ_REGS* irqRegs = NULL;
    XDMA_ENGINE* engine = NULL;

    irqRegs = xdma->interruptRegs;
    // dma engine interrupt pending?
    ULONGLONG currMs = KeQueryUnbiasedInterruptTime() / 10000;//100ns��λ
    TraceVerbose(DBG_DPC, "%!FUNC! channel CIP=0x%08X", irq->channelIrqPending);
	DbgPrint("channel CIP=0x%08X\n", irq->channelIrqPending);
	//DbgPrint(("channel CIP=0\n"));
    for (UINT dir = H2C; dir < 2; dir++) { // 0=H2C, 1=C2H
        for (UINT channel = 0; channel < XDMA_MAX_NUM_CHANNELS; channel++) {
            engine = &xdma->engines[channel][dir];
            if (engine->enabled && (irq->channelIrqPending & engine->irqBitMask)) {
                TraceInfo(DBG_DPC, "%!FUNC! Servicing %s engine channel #%d. queue=%p",
                          dir == C2H ? "C2H" : "H2C", channel, engine->queue);

				DbgPrint("Servicing %s engine channel #%d. queue=%p\n",
					dir == C2H ? "C2H" : "H2C", channel, engine->queue);
                ASSERT(engine->work != NULL);

                if ((currMs > engine->lastInterruptMs) && (currMs - engine->lastInterruptMs) >= 15) {
                    engine->work(engine);
                    engine->lastInterruptMs = currMs;
                }
            }
        }
    }

    // user event interrupt pending?
    TraceVerbose(DBG_DPC, "%!FUNC! user event pending = 0x%08X", irq->userIrqPending);
    for (UINT i = 0; i < XDMA_MAX_USER_IRQ; ++i) {
        if (irq->userIrqPending & BIT_N(i)) {
            WdfIoQueueStart(xdma->userEvents[i].queue); // wake the queue
        }
    }

    // re-enable interrupts
    WdfInterruptAcquireLock(interrupt);
    irqRegs->channelIntEnableW1S = irq->channelIrqPending;
    irq->channelIrqPending = 0x0;

    // FIXME ! BITSTREAM DEPENDANT ! - Remove the interrupt source condition
    // In the reference bitstream, user interrupts are triggered by writing to the first two bytes
    // in the user BAR, we need to remove the interrupt source condition by clearing the bits;
    if (xdma->userBarIdx >= 0) {
        PUINT32 userBarAddr = (PUINT32)xdma->bar[xdma->userBarIdx];
        *(userBarAddr + 0x00) = irq->userIrqPending;
    }


    irqRegs->userIntEnableW1S = irq->userIrqPending;
    irq->userIrqPending = 0x0;
    WdfInterruptReleaseLock(interrupt);
    TraceInfo(DBG_DPC, "%!FUNC! channel EN=0x%08X RQ=0x%08X PE=0x%08X",
              irqRegs->channelIntEnable, irqRegs->channelIntRequest, irqRegs->channelIntPending);
    TraceInfo(DBG_DPC, "%!FUNC! user EN=0x%08X RQ=0x%08X PE=0x%08X",
              irqRegs->userIntEnable, irqRegs->userIntRequest, irqRegs->userIntPending);
    return;
}

// ====================== MSI-X interrupt callback functions ======================================

NTSTATUS EvtChannelInterruptEnable(IN WDFINTERRUPT Interrupt, IN WDFDEVICE device) {
    UNREFERENCED_PARAMETER(device);
    PIRQ_CONTEXT irq = GetIrqContext(Interrupt);
    EngineEnableInterrupt(irq->engine);
    return STATUS_SUCCESS;
}

NTSTATUS EvtChannelInterruptDisable(IN WDFINTERRUPT Interrupt, IN WDFDEVICE device) {
    UNREFERENCED_PARAMETER(device); 
    PIRQ_CONTEXT irq = GetIrqContext(Interrupt);
    EngineDisableInterrupt(irq->engine);
    return STATUS_SUCCESS;
}

BOOLEAN EvtChannelInterruptIsr(IN WDFINTERRUPT Interrupt, IN ULONG MessageID)
// interrupt service routine - handles msi/msi-x interrupts only
{
    BOOLEAN success = FALSE;
    PXDMA_DEVICE xdma = GetDeviceContext(WdfInterruptGetDevice(Interrupt));
    PIRQ_CONTEXT irq = GetIrqContext(Interrupt);

    ASSERTMSG("isr irq context NULL", irq != NULL);
    ASSERTMSG("isr device context NULL", xdma != NULL);

    TraceInfo(DBG_IRQ, "%!FUNC! message id = %u", MessageID);

    // check if MessageId is an expected Channel/Engine irq message id
    EngineDisableInterrupt(irq->engine);

    // schedule deferred work
    success = WdfInterruptQueueDpcForIsr(Interrupt);
    TraceInfo(DBG_DPC, "%!FUNC! channel EN=0x%08X RQ=0x%08X PE=0x%08X",
              xdma->interruptRegs->channelIntEnable, xdma->interruptRegs->channelIntRequest,
              xdma->interruptRegs->channelIntPending);
    return success;
}

VOID EvtChannelInterruptDpc(IN WDFINTERRUPT interrupt, IN WDFOBJECT device)
// Deferred interrupt service handler
{
    PIRQ_CONTEXT irq = GetIrqContext(interrupt);
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    irq->engine->work(irq->engine);
    WdfInterruptAcquireLock(interrupt);
    EngineEnableInterrupt(irq->engine);
    WdfInterruptReleaseLock(interrupt);
    TraceInfo(DBG_DPC, "%!FUNC! channel EN=0x%08X RQ=0x%08X PE=0x%08X",
              xdma->interruptRegs->channelIntEnable, xdma->interruptRegs->channelIntRequest,
              xdma->interruptRegs->channelIntPending);
}

NTSTATUS EvtUserInterruptEnable(IN WDFINTERRUPT Interrupt, IN WDFDEVICE device) {
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    PIRQ_CONTEXT irq = GetIrqContext(Interrupt);
    ULONG eventId = irq->eventId; // message id and event id are same

    xdma->interruptRegs->userIntEnableW1S = BIT_N(eventId);
    TraceInfo(DBG_IRQ, "%!FUNC! userIntEnable=0x%08x", xdma->interruptRegs->userIntEnable);
    return STATUS_SUCCESS;
}

NTSTATUS EvtUserInterruptDisable(IN WDFINTERRUPT Interrupt, IN WDFDEVICE device) {
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    PIRQ_CONTEXT irq = GetIrqContext(Interrupt);
    ULONG eventId = irq->eventId; // message id and event id are same

    xdma->interruptRegs->userIntEnableW1C = BIT_N(eventId);
    TraceInfo(DBG_IRQ, "%!FUNC! userIntEnable=0x%08x", xdma->interruptRegs->userIntEnable);
    return STATUS_SUCCESS;
}

BOOLEAN EvtUserInterruptIsr(IN WDFINTERRUPT Interrupt, IN ULONG MessageID) {
    BOOLEAN success = FALSE;
    PXDMA_DEVICE xdma = GetDeviceContext(WdfInterruptGetDevice(Interrupt));
    TraceVerbose(DBG_DPC, "%!FUNC! user event %u occurred", MessageID);

    // disable user event interrupt
    xdma->interruptRegs->userIntEnableW1C = BIT_N(MessageID); // message id and event id are same
    TraceVerbose(DBG_DPC, "%!FUNC! userIntEnable=0x%08x", xdma->interruptRegs->userIntEnable);

    // schedule deferred work
    success = WdfInterruptQueueDpcForIsr(Interrupt);
    TraceInfo(DBG_DPC, "%!FUNC! user EN=0x%08X RQ=0x%08X PE=0x%08X",
              xdma->interruptRegs->userIntEnable, xdma->interruptRegs->userIntRequest,
              xdma->interruptRegs->userIntPending);
    return success;
}

VOID EvtUserInterruptDpc(IN WDFINTERRUPT interrupt, IN WDFOBJECT device)
// Deferred interrupt service handler
{
    PXDMA_DEVICE xdma = GetDeviceContext(device);
    PIRQ_CONTEXT irq = GetIrqContext(interrupt);

    ULONG eventId = irq->eventId; // message id and event id are same
                                  // user event interrupt pending?
    TraceVerbose(DBG_DPC, "%!FUNC! servicing user event %u", eventId);
    WdfIoQueueStart(xdma->userEvents[eventId].queue); // wake the queue

    WdfInterruptAcquireLock(interrupt);

    // FIXME ! BITSTREAM DEPENDANT ! - Remove the interrupt source condition
    // In the reference bitstream, user interrupts are triggered by writing to the first two bytes
    // in the user BAR, we need to remove the interrupt source condition by clearing the bit;
    //PUCHAR writeAddr = (PUCHAR)xdma->bar[xdma->userBarIdx];
    //*writeAddr &= ~(BIT_N(eventId));

    // reenable interrupt
    xdma->interruptRegs->userIntEnableW1S = BIT_N(eventId);

    WdfInterruptReleaseLock(interrupt);

    TraceInfo(DBG_DPC, "%!FUNC! user EN=0x%08X RQ=0x%08X PE=0x%08X",
              xdma->interruptRegs->userIntEnable, xdma->interruptRegs->userIntRequest,
              xdma->interruptRegs->userIntPending);
    return;
}
