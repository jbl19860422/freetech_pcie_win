﻿#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QSemaphore>
#include <QMainWindow>
#include <memory>
#include <QPushButton>
#include "pcie_card.h"
class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();

    void init();
private:
    void clickedStartBtn();
    void clickedStopBtn();
    void clickedCloseBtn();
private:

    QPushButton *btn_init_;
    QPushButton *btn_start_capture_;
    QPushButton *btn_stop_capture_;
    QPushButton *btn_uninit_;

    int frame_count_ = 0;

    std::shared_ptr<VideoCard> video_card_;
    bool capture_ = false;
};

#endif // MAINWINDOW_H
