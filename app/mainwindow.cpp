﻿#include "mainwindow.h"
#include <QVariant>
#include <QVBoxLayout>
#include <QCoreApplication>
#include <QPushButton>
#include <QApplication>
#include <QString>
#include <QDebug>
#include <QButtonGroup>
#include <QPainter>
#include <QPainterPath>
#include <QGraphicsDropShadowEffect>
#include <QLabel>
#include <QScreen>
#include <conio.h>
#include <QDateTime>
#define FRAMESHAPE 10

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    setSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
    //setMinimumSize(800, 600);
}

MainWindow::~MainWindow()
{
}

void MainWindow::init()
{
    QWidget *main_widget = new QWidget(this);
    QVBoxLayout *main_layout = new QVBoxLayout(this);
    main_layout->setContentsMargins(8, 8, 8, 8);
    main_layout->setSpacing(0);
    main_layout->setMargin(0);
    main_layout->setSizeConstraint(QLayout::SetMaximumSize);

    QLabel *title = new QLabel(QString::fromLocal8Bit("CaptureDemo"));
    title->setProperty("type", QVariant("nav_title"));

    QPushButton *btn_close = new QPushButton;
    btn_close->setProperty("type", QVariant("nav_close_btn"));
    btn_close->setCursor(Qt::PointingHandCursor);
    btn_close->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    btn_close->setFixedSize(20, 20);
    connect(btn_close, &QPushButton::clicked, this, &MainWindow::clickedCloseBtn);

    QHBoxLayout *nav_layout = new QHBoxLayout;
    nav_layout->setMargin(0);
    nav_layout->setSpacing(0);
    nav_layout->setContentsMargins(8, 0, 8, 0);
    nav_layout->addWidget(title, 0, Qt::AlignLeft | Qt::AlignVCenter);

    nav_layout->addStretch(9);
    nav_layout->addSpacing(10);
    nav_layout->addWidget(btn_close, 0, Qt::AlignRight | Qt::AlignVCenter);


    QWidget *btns_widget = new QWidget;
    QHBoxLayout *btns_layout = new QHBoxLayout;
    btns_layout->setMargin(0);
    btns_layout->setSpacing(4);
    btns_layout->setContentsMargins(0, 0, 0, 0);

    video_card_ = std::make_shared<VideoCard>();

    QButtonGroup *btn_group = new QButtonGroup;
    btn_init_ = new QPushButton(QString::fromLocal8Bit("打开板卡"));
    btn_init_->setCursor(Qt::PointingHandCursor);
    btn_init_->setFixedWidth(100);
    btn_init_->setEnabled(true);
    btns_layout->addWidget(btn_init_);
    connect(btn_init_, &QPushButton::clicked, [=]() {
        if(video_card_) {
            if(0 != video_card_->init()) {
                qDebug() << "video card init failed.";
                return;
            }

            btn_start_capture_->setEnabled(true);
            btn_init_->setEnabled(false);
            btn_uninit_->setEnabled(true);
        }
    });

    btn_start_capture_ = new QPushButton(QString::fromLocal8Bit("开始采集"));
    btn_start_capture_->setCursor(Qt::PointingHandCursor);
    btn_start_capture_->setEnabled(false);
    btns_layout->addWidget(btn_start_capture_);
    connect(btn_start_capture_, &QPushButton::clicked, [=]() {
        if(video_card_) {
            auto cb = [=](char *data, int size) {
                if (frame_count_ < 10) {
                    FILE *fp;
                    fp = fopen(("cap" + std::to_string(frame_count_) + ".bin").c_str(), "wb");
                    fwrite(data, 1, size, fp);
                    fclose(fp);
                    qDebug() << "capture " << frame_count_;
                }
                frame_count_++;
            };

            if(0 != video_card_->startCapture(0, cb)) {
                qDebug() << "video card startCapture failed.";
                return;
            }
            btn_stop_capture_->setEnabled(true);
            btn_start_capture_->setEnabled(false);
        }
    });

    btn_stop_capture_ = new QPushButton(QString::fromLocal8Bit("停止采集"));
    btn_stop_capture_->setCheckable(true);
    btn_stop_capture_->setCursor(Qt::PointingHandCursor);
    btn_stop_capture_->setFixedWidth(100);
    btn_stop_capture_->setEnabled(false);
    btns_layout->addWidget(btn_stop_capture_);
    connect(btn_stop_capture_, &QPushButton::clicked, [=]() {
        if(video_card_) {
            video_card_->stopCapture(0);
            btn_start_capture_->setEnabled(true);
            btn_stop_capture_->setEnabled(false);
        }
    });

    btn_uninit_ = new QPushButton(QString::fromLocal8Bit("关闭板卡"));
    btn_uninit_->setCheckable(true);
    btn_uninit_->setCursor(Qt::PointingHandCursor);
    btn_uninit_->setFixedWidth(100);
    btn_uninit_->setEnabled(false);
    btns_layout->addWidget(btn_uninit_);
    connect(btn_uninit_, &QPushButton::clicked, [=]() {
        if(video_card_) {
            video_card_->uninit();
            btn_init_->setEnabled(true);
            btn_uninit_->setEnabled(false);
            btn_start_capture_->setEnabled(false);
            btn_stop_capture_->setEnabled(false);
        }
    });

    btns_widget->setLayout(btns_layout);
    btns_widget->setMouseTracking(true);

    main_layout->addSpacing(5);
    main_layout->addWidget(btns_widget);

    main_widget->setLayout(main_layout);
    setCentralWidget(main_widget);
}

void MainWindow::clickedCloseBtn()
{

    if(video_card_) {
        video_card_->uninit();
        video_card_.reset();
        video_card_ = nullptr;
    }

    QCoreApplication::instance()->quit();
}


