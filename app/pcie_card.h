﻿#ifndef VIDEO_CARD_H_
#define VIDEO_CARD_H_
#include <memory>
#include <Windows.h>
#include <condition_variable>
#include <functional>
#include "common/semaphore.h"

#define H2C_CHANNEL_NUM 2
#define C2H_CHANNEL_NUM 2

class PCIECard
{
public:
    PCIECard();
    PCIECard(const PCIECard &card) = delete;
    PCIECard & operator=(const PCIECard &card) = delete;

    virtual ~PCIECard();
    /*
    * @fun：初始化
    * @return 0：成功，非0：失败
    */
    int init();
    /*
    * @fun：反初始化
    */
    void uninit();
    /*
    * @fun：开始收数据
    * @param[in] ch_index：通道号
    * @param[in] cb：回调函数
    * @return 0：成功，非0：失败
    */
    int startRecv(int ch_index, const std::function<void(char *, int size)> &cb);

    /*
    * @fun：停止采集
    * @param[in] ch_index：通道号
    */
    void stopRecv(int ch_index);

    /*
    * @fun：发送数据
    * @param[in] ch_index：通道号
    * @param[in] data：数据指针
    * @param[in] size：字节数
    * @return 0：成功，非0：失败
    */
    int send(int ch_index, char *data, size_t size);
private:
    /*
     * @fun：写用户空间寄存器
     */
    void writeUser(long address, unsigned int val);
    int  readUser(long address, unsigned int &val);

    int readEvent(int index, unsigned int &val);
private:
    int getDevices(GUID guid, char *devpath, size_t len_devpath);
    int openDevices();
    void closeDevices();
public:
    int writeDevice(HANDLE device, long address, DWORD size, BYTE *buffer);
    int readDevice(HANDLE device, long address, DWORD size, BYTE *buffer);
private:
    bool capturing_[C2H_CHANNEL_NUM] = {false};
    //接收数据使用
    unsigned char *c2h_align_mems_[C2H_CHANNEL_NUM];
    std::shared_ptr<std::thread> c2h_threads_[C2H_CHANNEL_NUM] = {nullptr};
    void c2hThread(int ch_index);
    std::shared_ptr<std::function<void(char*, int)>> recv_data_cb_[C2H_CHANNEL_NUM] = {nullptr};
private:
    bool is_initialized_ = false;

    HANDLE c2h_devices_[C2H_CHANNEL_NUM] = {NULL};
    HANDLE h2c_devices_[H2C_CHANNEL_NUM] = {NULL};
    HANDLE event_devices_[16] = {NULL};
    HANDLE user_device_ = NULL;
    uint32_t card_idx_;
};
#endif // PCIECard_H
