﻿#include <QDebug>
#include "pcie_card.h"

#include <SetupAPI.h>
#include <string.h>
#include <Initguid.h>
#include <iostream>
#include "common/utils.h"

#pragma comment(lib, "SetupAPI.lib")

// 74c7e4a9-6d5d-4a70-bc0d-20691dff9e9d
DEFINE_GUID(GUID_DEVINTERFACE_XDMA,
            0x74c7e4a9, 0x6d5d, 0x4a70, 0xbc, 0x0d, 0x20, 0x69, 0x1d, 0xff, 0x9e, 0x9d);

#define MAX_BYTES_PER_TRANSFER (8*1024*1024)
#define MAX_PATH 400
/*****************寄存器地址定义****************/
#define PCIE_DATE               0x00
#define PCIE_VERSION            0x04
#define TEST_REG1               0x08
#define TEST_REG2               0x0c
#define DMA_DOWN_READY          0x10    //DMA允许下发标志，1有效，表示可以允许驱动下发DMA数据。
#define DMA_DOWN_TIMEOUT        0x14    //DMA传输超时次数计数，DmaDownCountClear为1时清0。
#define DMA_DOWN_HEAD_ERRCOUNT  0x18    //DMA传输数据包头出错次数计数，DmaDownCount Clear为1时清0。
#define DMA_DOWN_COUNT_CLEAR    0x1c    //异常事件计数器清0，先写1再写0把内部异常事件计数器清0。

#define DMA_UP_INT_CLEAR        0x20    //上行DMA中断清除标志，1有效。
#define DMA_UP_LEN              0x24    //上行DMA数据包长度
#define DMA_UP_TIMEOUT_COUNT    0x28    //DMA传输超时次数计数，DmaUpCountClear为1时清0。
#define DMA_UP_COUNT_CLEAR      0x30    //异常事件计数器清0，先写1再写0把内部异常事件计数器清0。

// 包头
#define DATA_HEADER 0xa5a5a5a5

PCIECard::PCIECard()
{
    is_initialized_ = false;
}

PCIECard::~PCIECard()
{
    uninit();
}

int PCIECard::init()
{
    int code = openDevices();
    if(0 != code) {
        return -1;
    }

    is_initialized_ = true;
    return 0;
}

void PCIECard::uninit()
{
    if(!is_initialized_) {
        return;
    }

    for(int i = 0; i < C2H_CHANNEL_NUM; i++) {
        stopRecv(i);
    }

    is_initialized_ = false;
    closeDevices();
}

int PCIECard::startRecv(int ch_index, const std::function<void(char *, int)> &cb)
{
    if(!is_initialized_) {
        return -1;
    }

    if(capturing_[ch_index]) {
        return -2;
    }

    capturing_[ch_index] = true;
    recv_data_cb_[ch_index] = std::make_shared<std::function<void(char *, int)>>(cb);
    c2h_threads_[ch_index] = std::make_shared<std::thread>(std::bind(&PCIECard::c2hThread, this, ch_index));
    return 0;
}

void PCIECard::stopRecv(int ch_index)
{
    capturing_[ch_index] = false;

    if(c2h_threads_[ch_index]) {
        c2h_threads_[ch_index]->join();
        c2h_threads_[ch_index].reset();
    }

    recv_data_cb_[ch_index] = nullptr;
    Sleep(10);
}

void PCIECard::c2hThread(int ch_index)
{
    while (1)
    {
        if(!capturing_[video_index]) {
            break;
        }

        int event_index = ch_index;
        unsigned int val;
        if (0 != readEvent(event_index, val))
        {
            std::cout << "read event failed." << std::endl;
            continue;
        }
        //sem_post(&c2h_sems_[video_index]);

        unsigned int frame_size;
        if (video_index == 0) {
            readUser(C2H0_DATA_SIZE, frame_size);
        } else if (video_index == 1) {
            readUser(C2H1_DATA_SIZE, frame_size);
        } else if (video_index == 2) {
            readUser(C2H2_DATA_SIZE, frame_size);
        }

        std::cout << "video_index:" << video_index << ", frame_size:" << frame_size << std::endl;
        int size = readDevice(c2h_devices_[video_index], 0, frame_size, (BYTE*)c2h_align_mems_[video_index]);
        std::cout << "video_index:" << video_index << ", frame_size:" << frame_size << ", size:" << size << std::endl;
        if(size < 0) {
            std::cout << "read device failed, code:" << size;
            break;
        }
        //调用注册回调
        if(capturing_[video_index] && recv_data_cb_[video_index]) {
            (*recv_data_cb_[video_index])((char*)c2h_align_mems_[video_index], frame_size);
        } else {
            std::cout << "no cb" << std::endl;
        }
    }
}

void PCIECard::c2hDataThread(int video_index)
{
    // while(1) {
    //     sem_wait(&c2h_sems_[video_index]);
    //     if(!capturing_[video_index]) {
    //         break;
    //     }

    //     unsigned int frame_size;
    //     readUser(C2H_DATA_SIZE, frame_size);
    //     std::cout << "frame_size:" << frame_size << std::endl;
    //     int size = readDevice(c2h_devices_[video_index], 0, frame_size, (BYTE*)c2h_align_mems_[video_index]);
    //     if(size < 0) {
    //         break;
    //     }
    //     //调用注册回调
    //     if(capturing_[video_index] && recv_data_cb_[video_index]) {
    //         (*recv_data_cb_[video_index])((char*)c2h_align_mems_[video_index], frame_size);
    //     }
    // }
}

int PCIECard::startSending(int video_index, const std::function<void(int index)> & ready_cb)
{
    if(!is_initialized_) {
        return -1;
    }

    if(sending_[video_index]) {
        return -2;
    }

    send_ready_cb_[video_index] = std::make_shared<std::function<void(int)>>(ready_cb);
    sending_[video_index] = true;
    h2c_event_threads_[video_index] = std::make_shared<std::thread>(std::bind(&PCIECard::h2cEventThread, this, video_index));
    h2c_data_threads_[video_index] = std::make_shared<std::thread>(std::bind(&PCIECard::h2cDataThread, this, video_index));
    return 0;
}

void PCIECard::initSending(int video_index)
{
    if (!is_initialized_) {
        return;
    }
    writeUser(H2C_BASE + video_index*0x10 + 0x04, 0);//复位
    writeUser(H2C_BASE + video_index*0x10 + 0x04, 1);//复位
    writeUser(H2C_BASE + video_index*0x10 + 0x08, 1);//开始
}

void PCIECard::stopSending(int video_index)
{
    sending_[video_index] = false;
    h2c_sems_[video_index].signal();

    if(h2c_event_threads_[video_index]) {
        h2c_event_threads_[video_index]->join();
        h2c_event_threads_[video_index].reset();
    }

    if(h2c_data_threads_[video_index]) {
        if(h2c_data_threads_[video_index]->joinable()) {
            h2c_data_threads_[video_index]->join();
        }
        h2c_data_threads_[video_index].reset();
    }
    send_ready_cb_[video_index] = nullptr;
    Sleep(10);
}

void PCIECard::h2cEventThread(int video_index)
{
    writeUser(H2C_BASE + video_index*0x10 + 0x04, 0);//复位
    writeUser(H2C_BASE + video_index*0x10 + 0x04, 1);//复位
    writeUser(H2C_BASE + video_index*0x10 + 0x08, 1);//开始
    bool first = true;
    while (1)
    {
        if(!sending_[video_index]) {
            break;
        }

        if (first) {
            h2c_sems_[video_index].signal();
            first = false;
            continue;
        }
        uint32_t val;
        int event_index;
        if (video_index == 0) {
            event_index = 2;
        } else {
            event_index = 3;
        }
        if (0 != readEvent(event_index, val))
        {
            std::cout << "read event failed." << std::endl;
        }
        h2c_sems_[video_index].signal();
    }
}

void PCIECard::h2cDataThread(int video_index)
{
    while(1) {
        if(!sending_[video_index]) {
            break;
        }

        h2c_sems_[video_index].wait();
        uint32_t data_num;
        readUser(H2C_BASE + video_index*0x10 + 0x0c, data_num);
        (*send_ready_cb_[video_index])(video_index);
    }
}

int PCIECard::sendImg(int video_index, std::unique_ptr<char[]> data, size_t len) {
    memcpy(h2c_align_mems_[video_index], data.get(), len);
    if (video_index == 0) {
        writeUser(H2C_START0, 1);
    } else {
        writeUser(H2C_START1, 1);
    }

    auto size = writeDevice(h2c_devices_[video_index],0, len, h2c_align_mems_[video_index]);
    if (video_index == 0) {
        writeUser(H2C_START0, 0);
    } else {
        writeUser(H2C_START1, 0);
    }
    return size;
}

int  PCIECard:: readUser(long address, unsigned int &val)
{
    int ret = readDevice(user_device_, address, sizeof(val), (BYTE*)&val);
    if (0 != ret) {
        return -1;
    }
    return 0;
}

int PCIECard::readDevice(HANDLE device, long address, DWORD size, BYTE *buffer)
{
    DWORD rd_size = 0;

    unsigned int transfers;
    int i;
    if (INVALID_SET_FILE_POINTER == SetFilePointer(device, address, NULL, FILE_BEGIN))
    {
        fprintf(stderr, "Error setting file pointer, win32 error code: %ld\n", GetLastError());
        return -3;
    }
    transfers = (unsigned int)(size / MAX_BYTES_PER_TRANSFER);
    for (i = 0; i < transfers; i++)
    {
        if (!ReadFile(device, (void *)(buffer + i * MAX_BYTES_PER_TRANSFER), (DWORD)MAX_BYTES_PER_TRANSFER, &rd_size, NULL))
        {
            return -1;
        }
        if (rd_size != MAX_BYTES_PER_TRANSFER)
        {
            return -2;
        }
    }
    if (!ReadFile(device, (void *)(buffer + i * MAX_BYTES_PER_TRANSFER), (DWORD)(size - i * MAX_BYTES_PER_TRANSFER), &rd_size, NULL))
    {
        return -4;
    }
    if (rd_size != (size - i * MAX_BYTES_PER_TRANSFER))
    {
        return -5;
    }

    return size;
}

void PCIECard::writeUser(long address, unsigned int val)
{
    writeDevice(user_device_, address, sizeof(unsigned int), (BYTE *)&val);
}

int PCIECard::readEvent(int index, unsigned int &val)
{
    BYTE val1;
    int ret = readDevice(event_devices_[index], 0, 1, (BYTE*)&val1);
    if (ret > 0) {
        return 0;
    }
    return -1;
}

int PCIECard::writeDevice(HANDLE device, long address, DWORD size, BYTE *buffer)
{
    DWORD wr_size = 0;
    unsigned int transfers;
    int i;
    transfers = (unsigned int)(size / MAX_BYTES_PER_TRANSFER);
    if (INVALID_SET_FILE_POINTER == SetFilePointer(device, address, NULL, FILE_BEGIN))
    {
        fprintf(stderr, "Error setting file pointer, win32 error code: %ld\n", GetLastError());
        return -1;
    }

    for (i = 0; i < transfers; i++)
    {
        if (!WriteFile(device, (void *)(buffer + i * MAX_BYTES_PER_TRANSFER), MAX_BYTES_PER_TRANSFER, &wr_size, NULL))
        {
            return -1;
        }
        if (wr_size != MAX_BYTES_PER_TRANSFER)
        {
            return -2;
        }
    }
    if (!WriteFile(device, (void *)(buffer + i * MAX_BYTES_PER_TRANSFER), (DWORD)(size - i * MAX_BYTES_PER_TRANSFER), &wr_size, NULL))
    {
        return -1;
    }
    if (wr_size != (size - i * MAX_BYTES_PER_TRANSFER))
    {
        return -2;
    }
    return size;
}

int PCIECard::getDevices(GUID guid, char *devpath, size_t len_devpath)
{
    SP_DEVICE_INTERFACE_DATA device_interface;
    PSP_DEVICE_INTERFACE_DETAIL_DATA dev_detail;
    DWORD index;
    HDEVINFO device_info;
    wchar_t tmp[256];
    device_info = SetupDiGetClassDevs((LPGUID)&guid, NULL, NULL, DIGCF_PRESENT | DIGCF_DEVICEINTERFACE);
    if (device_info == INVALID_HANDLE_VALUE)
    {
        fprintf(stderr, "GetDevices INVALID_HANDLE_VALUE\n");
        return -1;
    }

    device_interface.cbSize = sizeof(SP_DEVICE_INTERFACE_DATA);
    // enumerate through devices

    for (index = 0; SetupDiEnumDeviceInterfaces(device_info, NULL, &guid, index, &device_interface); ++index)
    {

        // get required buffer size
        ULONG detailLength = 0;
        if (!SetupDiGetDeviceInterfaceDetail(device_info, &device_interface, NULL, 0, &detailLength, NULL) && GetLastError() != ERROR_INSUFFICIENT_BUFFER)
        {
            fprintf(stderr, "SetupDiGetDeviceInterfaceDetail - get length failed\n");
            break;
        }

        // allocate space for device interface detail
        dev_detail = (PSP_DEVICE_INTERFACE_DETAIL_DATA)HeapAlloc(GetProcessHeap(), HEAP_ZERO_MEMORY, detailLength);
        if (!dev_detail)
        {
            fprintf(stderr, "HeapAlloc failed\n");
            break;
        }
        dev_detail->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

        // get device interface detail
        if (!SetupDiGetDeviceInterfaceDetail(device_info, &device_interface, dev_detail, detailLength, NULL, NULL))
        {
            fprintf(stderr, "SetupDiGetDeviceInterfaceDetail - get detail failed\n");
            HeapFree(GetProcessHeap(), 0, dev_detail);
            break;
        }

        wcscpy(tmp, dev_detail->DevicePath);
        wcstombs(devpath, tmp, 256);
        HeapFree(GetProcessHeap(), 0, dev_detail);
    }

    SetupDiDestroyDeviceInfoList(device_info);

    return index;
}

int PCIECard::openDevices()
{
    char device_path[MAX_PATH + 1] = "";
    char device_base_path[MAX_PATH + 1] = "";
    wchar_t device_path_w[MAX_PATH + 1] ;
    DWORD num_devices = getDevices(GUID_DEVINTERFACE_XDMA, device_base_path, sizeof(device_base_path));

    if (num_devices < 1)
    {
        printf("error\n");
        return -1;
    }
    // extend device path to include target device node (xdma_control, xdma_user etc)

    for (int i = 0; i < C2H_CHANNEL_NUM; i++) {
        memset(device_path,0,sizeof(device_path));
        strcpy_s(device_path, sizeof device_path, device_base_path);
        strcat_s(device_path, sizeof device_path, "\\");
        char buf[256] = {0};
        sprintf(buf, "c2h_%d", i);
        strcat_s(device_path, sizeof device_path, buf);

        // open device file
        mbstowcs(device_path_w, device_path,sizeof(device_path));
        c2h_devices_[i] = CreateFile(device_path_w, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (c2h_devices_[i] == INVALID_HANDLE_VALUE)
        {
            fprintf(stderr, "Error opening device, win32 error code: %ld\n", GetLastError());
            return -1;
        }
    }


    for (int i = 0; i < H2C_CHANNEL_NUM; i++) {
        memset(device_path,0,sizeof(device_path));
        strcpy_s(device_path, sizeof device_path, device_base_path);
        strcat_s(device_path, sizeof device_path, "\\");
        char buf[256] = {0};
        sprintf(buf, "h2c_%d", i);
        strcat_s(device_path, sizeof device_path, buf);

        // open device file
        mbstowcs(device_path_w, device_path,sizeof(device_path));
        h2c_devices_[i] = CreateFile(device_path_w, GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (h2c_devices_[i] == INVALID_HANDLE_VALUE)
        {
            fprintf(stderr, "Error opening device, win32 error code: %ld\n", GetLastError());
            return -2;
        }
    }

    memset(device_path,0,sizeof(device_path));
    strcpy_s(device_path, sizeof device_path, device_base_path);
    strcat_s(device_path, sizeof device_path, "\\");
    strcat_s(device_path, sizeof device_path, "user");
    // open device file
    mbstowcs(device_path_w, device_path, sizeof(device_path));
    user_device_ = CreateFile(device_path_w, GENERIC_READ | GENERIC_WRITE, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
    if (user_device_ == INVALID_HANDLE_VALUE)
    {
        fprintf(stderr, "Error opening device, win32 error code: %ld\n", GetLastError());
        return -3;
    }

    for (int i = 0; i < 16; i++) {
        memset(device_path,0,sizeof(device_path));
        strcpy_s(device_path, sizeof device_path, device_base_path);
        strcat_s(device_path, sizeof device_path, "\\");
        char buf[256] = {0};
        sprintf(buf, "event_%d", i);
        strcat_s(device_path, sizeof device_path, buf);
        // open device file
        mbstowcs(device_path_w, device_path, sizeof(device_path));
        event_devices_[i] = CreateFile(device_path_w, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
        if (event_devices_[i] == INVALID_HANDLE_VALUE)
        {
            fprintf(stderr, "Error opening device, win32 error code: %ld\n", GetLastError());
            return -4;
        }
    }

    return 0;
}

void PCIECard::closeDevices()
{
    for(int i = 0; i < 16; i++) {
        CloseHandle(event_devices_[i]);
    }

    CloseHandle(user_device_);

    for(int i = 0; i < C2H_CHANNEL_NUM; i++) {
        CloseHandle(c2h_devices_[i]);
    }

    for(int i = 0; i < H2C_CHANNEL_NUM; i++) {
        CloseHandle(h2c_devices_[i]);
    }
}
